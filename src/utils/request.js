/* eslint-disable import/no-cycle */
// /**
//  * Parses the JSON returned by a network request
//  *
//  * @param  {object} response A response from a network request
//  *
//  * @return {object}          The parsed JSON from the request
//  */
// import * as modalActions from '../containers/Modals/actions';
// import * as globalActions from '../containers/App/actions';
// import { store } from '../configureStore';
import { toast } from 'react-toastify';

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response.json();
  }

  // if (response.status === 401) {
  //   if (getAccessToken()) {
  //     store.dispatch(globalActions.logout());
  //   } else {
  //     toast.error('Your access right is expired, please relogin!');
  //     window.setTimeout(
  //       () => store.dispatch(modalActions.showLoginModals(true)),
  //       1000,
  //     );
  //   }
  // }

  if (response.status >= 500) {
    toast.error('Không kết nối được đến server');
  }

  // const error = new Error(response.status);
  // error.response = response;
  throw response;
}

function checkStatuesParsedResponse(parsedResponse) {
  if (parsedResponse.status >= 200 && parsedResponse.status < 300) {
    return parsedResponse;
  }

  if (parsedResponse.status >= 400) {
    throw parsedResponse;
  }

  if (parsedResponse.status >= 500) {
    toast.error('Không kết nối được đến server');
  }

  throw parsedResponse;
}

/**
 * Requests a URL, returning a promise
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 *
 * @return {object}           The response data
 */

export async function storeToken(token) {
  try {
    await localStorage.setItem('access_token', token);
  } catch (error) {
    toast.warn('Cannot verify your account at this time!');
  }
}

export function getAccessToken() {
  try {
    return localStorage.getItem('access_token');
  } catch (err) {
    return null;
  }
}

export default function request(url, options) {
  const accessToken = getAccessToken();
  const parsedOptions = {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `${accessToken}`,
    },
    ...options,
  };
  return fetch(url, parsedOptions)
    .then(checkStatus)
    .then(checkStatuesParsedResponse);
}

// export function uploadFile(url, options) {
// const access_token = getAccessToken();
// const parsedOptions = Object.assign(
//   {
//     headers: {
//       Authorization: 'Client-ID 7f42b70a62b5759',
//     },
//   },
//   options,
// );
// return fetch(url, parsedOptions)
//   .then(checkStatus)
//   .then(checkStatuesParsedResponse);
// .then(parseJSON);
// }
