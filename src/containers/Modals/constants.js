/*
 *
 * Modals constants
 *
 */

export const SHOW_LOGIN_MODALS = 'w/Modals/SHOW_LOGIN_MODALS';

export const SHOW_REGISTER_MODALS = 'w/Modals/SHOW_REGISTER_MODALS';
