/*
 *
 * Modals reducer
 *
 */
import produce from 'immer';
import * as constants from './constants';

export const initialState = {
  showLoginModals: false,
  showRegisterModals: false,
};

/* eslint-disable default-case, no-param-reassign */
const modalsReducer = (state = initialState, action) =>
  produce(state, (newState) => {
    switch (action.type) {
      case constants.SHOW_LOGIN_MODALS:
        // console.log(action);
        newState.showLoginModals = action.status;
        break;

      case constants.SHOW_REGISTER_MODALS:
        // console.log(action);
        newState.showRegisterModals = action.status;
        break;

      default:
        break;
    }
  });

export default modalsReducer;
