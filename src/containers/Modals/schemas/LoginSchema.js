import * as Yup from 'yup';

const LoginSchema = () => {
  let validateShape = Yup.object();
  validateShape = Yup.object().shape({
    email: Yup.string().email('Invalid Email').required('Email is required'),
    password: Yup.string()
      .min(5, 'Password is too short!')
      .max(12, 'Password is too long!')
      .required('Password is required'),
  });
  return validateShape;
};

export default LoginSchema;
