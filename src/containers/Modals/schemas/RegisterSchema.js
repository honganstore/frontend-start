import * as Yup from 'yup';

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

const ResgisterSchema = () => {
  let validateShape = Yup.object();
  validateShape = Yup.object().shape({
    lastName: Yup.string().required('Họ không được để trống.'),
    firstName: Yup.string().required('Tên không được để trống.'),
    phone: Yup.string()
      .required('Số điện thoại không được để trống.')
      .min(10, 'Số điện thoại ít nhất 10 số.')
      .max(11, 'Số điện thoại tối đa 11 số.')
      .matches(phoneRegExp, 'Số điện thoại không hợp lệ.'),
    address: Yup.string().nullable(),
    email: Yup.string()
      .email('Địa chỉ email không hợp lệ.')
      .required('Email không được để trống.'),
    password: Yup.string()
      .required('Mật khẩu không được để trống')
      .matches(
        /^(?=.*[a-z])(?=.*[0-9])(?=.{8,})/,
        'Mật khẩu tối thiểu tám ký tự, bắt buộc bao gồm chữ cái và số.',
      ),
    confirmPassword: Yup.string()
      .required('Mật khẩu không được để trống.')
      .oneOf(
        [Yup.ref('password'), null],
        'Mật khẩu nhập lại không trùng khớp.',
      ),
  });
  return validateShape;
};

export default ResgisterSchema;
