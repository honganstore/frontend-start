import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the modals state domain
 */

const selectModalsDomain = (state) => state.modals || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Home
 */

const makeSelectModals = () =>
  createSelector(selectModalsDomain, (substate) => substate);

export const makeSelectCategory = () =>
  createSelector(selectModalsDomain, (substate) => substate.categories);

export const makeSelectShowLoginModals = () =>
  createSelector(selectModalsDomain, (substate) => substate.showLoginModals);

export const makeSelectShowRegisterModals = () =>
  createSelector(selectModalsDomain, (substate) => substate.showRegisterModals);

export default makeSelectModals;
export { selectModalsDomain };
