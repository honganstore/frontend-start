/*
 *
 * Modals actions
 *
 */

import * as constants from './constants';

export function showLoginModals(status) {
  return {
    type: constants.SHOW_LOGIN_MODALS,
    status,
  };
}

export function showRegisterModals(status) {
  return {
    type: constants.SHOW_REGISTER_MODALS,
    status,
  };
}
