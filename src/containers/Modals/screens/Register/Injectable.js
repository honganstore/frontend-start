/* eslint-disable consistent-return */
import { compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { withFormik } from 'formik';

import _ from 'lodash';
import RegisterSchema from '../../schemas/RegisterSchema';
import RegisterFormToRequest from '../../mappers/RegisterFormToRequest';

import RegisterModals from '.';

import * as appActions from '../../../App/actions';

const mapStateToProps = createStructuredSelector({});

function mapDispactchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispactchToProps);

export default compose(
  withConnect,
  withFormik({
    mapPropsToValues: () => ({
      firstName: 'Dung',
      lastName: 'Do',
      email: 'admin@mail.com',
      phone: '0121454784',
      address: 'home',
      password: 'adminabc123',
      confirmPassword: 'adminabc123',
    }),
    validate: async (values) => {
      const newRegisterSchema = RegisterSchema(values);
      const errors = {};
      try {
        await newRegisterSchema.validate(values, {
          abortEarly: false,
        });
      } catch (validationError) {
        _.each(validationError.inner, (error) => {
          _.set(errors, `${error.path}`, error.message);
        });

        return errors;
      }
    },
    handleSubmit: (values, formik) => {
      const { props } = formik;
      const { dispatch } = props;
      const request = RegisterFormToRequest(values);
      // console.log(values);
      // console.log(request);
      dispatch(appActions.register(request));
    },
  }),
)(RegisterModals);
