import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { Row, Col, Button, Modal, ModalHeader, ModalBody } from 'reactstrap';

import _ from 'lodash';

import FormInput from '../../../../components/Form/FormInput';

import * as actions from '../../actions';

const StyledModal = styled(Modal)`
  height: 100%;
  display: flex;
  align-items: center;
  margin: 30px auto;
  .modal-content {
    background-color: #faf9f8;
    border-radius: 0px !important;
    .modal-header {
      border-bottom: unset;
      .close {
        font-size: 54px;
        font-weight: 100;
        padding: 0 1rem;
        &:focus {
          outline: none;
        }
      }
      .modal-title {
        width: 100%;
        text-align: center;
      }
    }
  }
`;

const ModalWrapper = styled.div`
  padding: 12px 64px;
`;

const ModalHeaderWrapper = styled.div`
  width: 100%;
  .excerpt {
    text-align: center;
    font-size: 14px;
    margin: 0 0 20px;
  }
`;

const ModalTitle = styled.h1`
  font-size: 20px;
  line-height: 24px;
  background-position: top center;
  background-size: 90px;
  margin: 0 0 12px;
  text-align: center;
  text-transform: uppercase;
`;

const FooterLoginWrapper = styled.div`
  width: 100%;
  margin: 15px 0 0 0;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
`;
const ModalButton = styled(Button)`
  width: 100%;
  background-color: black;
  color: white;
  border-color: unset;
  margin: 10px 0 0 0;
  padding: 10px;
  text-transform: uppercase;
  &:hover,
  &:active,
  &:focus {
    background-color: black !important;
    color: white;
    border-color: unset;
    box-shadow: none;
  }
`;

const RegisterLink = styled(ModalButton)`
  width: 100%;
  background-color: white;
  color: black;
`;

class RegisterModals extends React.Component {
  closeModal = () => {
    this.props.dispatch(actions.showRegisterModals(false));
  };

  handleLoginModal = () => {
    this.props.dispatch(actions.showLoginModals(true));
    this.props.dispatch(actions.showRegisterModals(false));
  };

  render() {
    const { buttonLabel, className } = this.props;

    const { values, handleChange, touched, errors, handleSubmit } = this.props;

    const closeBtn = (
      <button type="button" className="close" onClick={this.closeModal}>
        &times;
      </button>
    );
    // console.log(this.props);
    return (
      <div>
        <Button color="danger" onClick={this.closeModal}>
          {buttonLabel}
        </Button>
        <StyledModal isOpen toggle={this.closeModal} className={className}>
          <ModalHeader toggle={this.closeModal} close={closeBtn} />
          <ModalWrapper>
            <ModalHeaderWrapper>
              <ModalTitle>Đăng ký</ModalTitle>
              <p className="excerpt">
                Trở thành thành viên - để không bỏ lỡ các ưu đãi, giảm giá,
                phiếu quà tặng
              </p>
            </ModalHeaderWrapper>
            <ModalBody>
              <Row>
                <Col xs={6}>
                  <FormInput
                    require
                    name="lastName"
                    title="Họ"
                    type="lastName"
                    placeholder="Họ"
                    value={values.lastName}
                    touched={_.get(touched, 'lastName')}
                    onChange={handleChange('lastName')}
                    errorMessage={_.get(errors, 'lastName', '')}
                  />
                </Col>
                <Col xs={6}>
                  <FormInput
                    require
                    name="firstName"
                    title="Tên"
                    type="firstName"
                    placeholder="Tên"
                    value={values.firstName}
                    touched={_.get(touched, 'firstName')}
                    onChange={handleChange('firstName')}
                    errorMessage={_.get(errors, 'firstName', '')}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <FormInput
                    require
                    name="email"
                    title="Email"
                    type="text"
                    placeholder="Email"
                    value={values.email}
                    touched={_.get(touched, 'email')}
                    onChange={handleChange('email')}
                    errorMessage={_.get(errors, 'email', '')}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <FormInput
                    require
                    name="phone"
                    title="Số điện thoại"
                    type="phone"
                    placeholder="Số điện thoại"
                    value={values.phone}
                    touched={_.get(touched, 'phone')}
                    onChange={handleChange('phone')}
                    errorMessage={_.get(errors, 'phone', '')}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <FormInput
                    name="address"
                    title="Địa chỉ"
                    type="address"
                    placeholder="Địa chỉ"
                    value={values.address}
                    touched={_.get(touched, 'address')}
                    onChange={handleChange('address')}
                    errorMessage={_.get(errors, 'address', '')}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <FormInput
                    require
                    name="password"
                    title="Mật khẩu"
                    type="password"
                    placeholder="Mật khẩu"
                    value={values.password}
                    touched={_.get(touched, 'password')}
                    onChange={handleChange('password')}
                    errorMessage={_.get(errors, 'password', '')}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <FormInput
                    require
                    name="confirmPassword"
                    title="Xác nhận mật khẩu"
                    type="password"
                    placeholder="Xác nhận mật khẩu"
                    value={values.confirmPassword}
                    touched={_.get(touched, 'confirmPassword')}
                    onChange={handleChange('confirmPassword')}
                    errorMessage={_.get(errors, 'confirmPassword', '')}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <FooterLoginWrapper>
                    <ModalButton onClick={handleSubmit}>Đăng ký</ModalButton>
                    <RegisterLink onClick={this.handleLoginModal}>
                      Đăng nhập
                    </RegisterLink>
                  </FooterLoginWrapper>
                </Col>
              </Row>
            </ModalBody>
          </ModalWrapper>
        </StyledModal>
      </div>
    );
  }
}

RegisterModals.propTypes = {
  dispatch: PropTypes.func,
  email: PropTypes.any,
  buttonLabel: PropTypes.any,
  className: PropTypes.any,
  values: PropTypes.any,
  handleChange: PropTypes.any,
  touched: PropTypes.any,
  errors: PropTypes.any,
  handleSubmit: PropTypes.func,
};

export default RegisterModals;
