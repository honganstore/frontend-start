/* eslint-disable consistent-return */
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { withFormik } from 'formik';

import _ from 'lodash';
import LoginFormToRequest from '../../mappers/LoginFormToRequest';
import LoginSchema from '../../schemas/LoginSchema';

import LoginModals from '.';

import * as appActions from '../../../App/actions';

const mapStateToProps = createStructuredSelector({});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
  withFormik({
    mapPropsToValues: () => ({
      email: 'admin@gmail.com',
      password: '12345678',
    }),
    validate: async (values) => {
      const newLoginSchema = LoginSchema(values);
      const errors = {};
      try {
        await newLoginSchema.validate(values, {
          abortEarly: false,
        });
      } catch (validationError) {
        _.each(validationError.inner, (error) => {
          _.set(errors, `${error.path}`, error.message);
        });

        return errors;
      }
    },
    handleSubmit: (values, formik) => {
      const { props } = formik;
      const { dispatch } = props;
      const requestData = LoginFormToRequest(values);
      // console.log(values);
      // console.log(requestData);
      dispatch(appActions.login(requestData));
    },
  }),
)(LoginModals);
