import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { Row, Col, Button, Modal, ModalHeader, ModalBody } from 'reactstrap';

import _ from 'lodash';

import FormInput from '../../../../components/Form/FormInput';

import * as actions from '../../actions';

const StyledModal = styled(Modal)`
  height: 100%;
  display: flex;
  align-items: center;
  margin: 0 auto;
  .modal-content {
    background-color: #faf9f8;
    border-radius: 0px !important;
    .modal-header {
      border-bottom: unset;
      .close {
        font-size: 54px;
        font-weight: 100;
        padding: 0 1rem;
        &:focus {
          outline: none;
        }
      }
      .modal-title {
        width: 100%;
        text-align: center;
      }
    }
  }
`;

const ModalWrapper = styled.div`
  padding: 12px 64px;
`;

const ModalHeaderWrapper = styled.div`
  width: 100%;
  .excerpt {
    text-align: center;
    font-size: 14px;
    margin: 0 0 20px;
  }
`;

const ModalTitle = styled.h1`
  font-size: 20px;
  line-height: 24px;
  background-position: top center;
  background-size: 90px;
  margin: 0 0 12px;
  text-align: center;
  text-transform: uppercase;
`;

const FooterLoginWrapper = styled.div`
  width: 100%;
  margin: 15px 0 0 0;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
`;
const ModalButton = styled(Button)`
  width: 100%;
  background-color: black;
  color: white;
  border-color: unset;
  margin: 10px 0 0 0;
  padding: 10px;
  text-transform: uppercase;
  &:hover,
  &:active,
  &:focus {
    background-color: black !important;
    color: white;
    border-color: unset;
    box-shadow: none;
  }
`;

const RegisterLink = styled(ModalButton)`
  width: 100%;
  background-color: white;
  color: black;
`;

class LoginModals extends React.Component {
  closeModal = () => {
    this.props.dispatch(actions.showLoginModals(false));
  };

  handleRegister = () => {
    this.props.dispatch(actions.showRegisterModals(true));
    this.props.dispatch(actions.showLoginModals(false));
  };

  render() {
    const { buttonLabel, className } = this.props;

    const { values, handleChange, touched, errors, handleSubmit } = this.props;

    const closeBtn = (
      <button type="button" className="close" onClick={this.closeModal}>
        &times;
      </button>
    );
    return (
      <div>
        <Button color="danger" onClick={this.closeModal}>
          {buttonLabel}
        </Button>
        <StyledModal isOpen toggle={this.closeModal} className={className}>
          <ModalHeader toggle={this.closeModal} close={closeBtn} />
          <ModalWrapper>
            <ModalHeaderWrapper>
              <ModalTitle>Đăng nhập</ModalTitle>
              <p className="excerpt">
                Trở thành thành viên - để không bỏ lỡ các ưu đãi, giảm giá,
                phiếu quà tặng
              </p>
            </ModalHeaderWrapper>
            <ModalBody>
              <Row>
                <Col>
                  <FormInput
                    require
                    name="email"
                    title="Email"
                    type="text"
                    placeholder="Email"
                    value={values.email}
                    touched={_.get(touched, 'email')}
                    onChange={handleChange('email')}
                    errorMessage={_.get(errors, 'email', '')}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <FormInput
                    require
                    name="password"
                    title="Mật khẩu"
                    type="password"
                    placeholder="Password"
                    value={values.password}
                    touched={_.get(touched, 'password')}
                    onChange={handleChange('password')}
                    errorMessage={_.get(errors, 'password', '')}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <FooterLoginWrapper>
                    <ModalButton onClick={handleSubmit}>Đăng nhập</ModalButton>
                    <RegisterLink onClick={this.handleRegister}>
                      Đăng ký
                    </RegisterLink>
                  </FooterLoginWrapper>
                </Col>
              </Row>
            </ModalBody>
          </ModalWrapper>
        </StyledModal>
      </div>
    );
  }
}

LoginModals.propTypes = {
  dispatch: PropTypes.func,
  email: PropTypes.any,
  buttonLabel: PropTypes.any,
  className: PropTypes.any,
  values: PropTypes.any,
  handleChange: PropTypes.any,
  touched: PropTypes.any,
  errors: PropTypes.any,
  handleSubmit: PropTypes.func,
};

export default LoginModals;
