import _ from 'lodash';

const RegisterFormToRequest = (values) => {
  const requestData = {
    first_name: values.firstName,
    last_name: values.lastName,
    phone: values.phone,
    address: values.address,
    email: values.email,
    password: values.password,
  };

  _.each(requestData, (value, key) => {
    if (!value && typeof value !== 'boolean' && typeof value !== 'number') {
      delete requestData[key];
    }
  });
  return requestData;
};

export default RegisterFormToRequest;
