import _ from 'lodash';

const LoginFormToRequest = (values) => {
  const requestData = {
    email: values.email,
    password: values.password,
  };

  _.each(requestData, (value, key) => {
    if (!value && typeof value !== 'boolean' && typeof value !== 'number') {
      delete requestData[key];
    }
  });

  return requestData;
};

export default LoginFormToRequest;
