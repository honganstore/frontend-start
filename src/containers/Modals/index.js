/* eslint-disable react/prop-types */
/**
 *
 * Home
 *
 */

import React from 'react';

const LoginModals = React.lazy(() => import('./screens/Login/Injectable'));
const RegisterModals = React.lazy(() =>
  import('./screens/Register/Injectable'),
);

function Modals(props) {
  const { showLoginModals, showRegisterModals } = props;

  return (
    <>
      {showLoginModals ? <LoginModals /> : null}
      {showRegisterModals ? <RegisterModals /> : null}
    </>
  );
}

export default React.memo(Modals);
