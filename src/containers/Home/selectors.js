import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the home state domain
 */

const selectHomeDomain = (state) => state.home || initialState;
const selectGlobalDomain = (state) => state.global || initialState;
/**
 * Other specific selectors
 */

/**
 * Default selector used by Home
 */

const makeSelectHome = () =>
  createSelector(selectHomeDomain, (substate) => substate);

export const makeSelectCategory = () =>
  createSelector(selectHomeDomain, (substate) => substate.categories);

export const makeSelectIsLogin = () =>
  createSelector(selectGlobalDomain, (substate) => substate.isLogin);

export const makeSelectCurrentUser = () =>
  createSelector(selectGlobalDomain, (substate) => substate.currentUser);

export default makeSelectHome;
export { selectHomeDomain };
