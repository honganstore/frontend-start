/*
 *
 * Home actions
 *
 */
import * as constants from './constants';

export function getJobList(filter = {}) {
  return {
    type: constants.GET_JOB_LIST,
    filter,
  };
}

export function getJobListSuccess(payload) {
  return {
    type: constants.GET_JOB_LIST_SUCCESS,
    payload,
  };
}

export function getJobListFailed(error) {
  return {
    type: constants.GET_JOB_LIST_FAILED,
    error,
  };
}

export function getCategory() {
  return {
    type: constants.GET_CATEGORY,
  };
}

export function getCategorySuccess(payload) {
  return {
    type: constants.GET_CATEGORY_SUCCESS,
    payload,
  };
}

export function getCategoryFailed(error) {
  return {
    type: constants.GET_CATEGORY_FAILED,
    error,
  };
}
