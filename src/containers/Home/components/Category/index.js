/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import _ from 'lodash';
import stylesDBL from '../../../../assets/main/maintainDBL.module.css';
import styles from '../../../../assets/main/maintainGYPC.module.css';

class Category extends React.Component {
  render() {
    // const { categories } = this.props;
    return (
      <>
        <section className={stylesDBL.maintainFrame}>
          <div className={stylesDBL.maintainTitle}>
            ĐỪNG BỎ LỠ DEAL HOT TRONG NGÀY
          </div>
          <div className={stylesDBL.maintain}>
            {_.get(this.props, 'categories.items[0].section', []).map(
              (children) => (
                <div key={children.title} className={stylesDBL.maintainEl}>
                  <div className={stylesDBL.maintainElWord}>
                    <p className={stylesDBL.maintainElSubtitle}>
                      {children.subTitle}
                    </p>
                    <p className={stylesDBL.maintainElTitle}>
                      {children.title}
                    </p>
                    <p className={stylesDBL.maintainElDiscription}>
                      {children.discription}
                    </p>
                  </div>
                  <div className={stylesDBL.maintainElImg}>
                    <img src={children.img} alt="Image_product" />
                    <div className={stylesDBL.maintainElCaret}>
                      <i className={children.childrencaret}></i>
                    </div>
                  </div>
                </div>
              ),
            )}
          </div>
        </section>
        <section>
          <div className={styles.maintain}>
            <div className={styles.maintainTitle}>Gợi Ý Phong Cách</div>
            <div className={styles.maintainFrame}>
              {_.get(this.props, 'categories.items[1].section', []).map(
                (item) => (
                  <div
                    key={item.title}
                    className={styles.maintainChildren}
                    style={item.bgItem}
                  >
                    <div>
                      <p className={`${styles.maintainChildrenSubTitle}`}>
                        {item.subTitle}
                      </p>
                      <p className={styles.maintainChildrenTitle}>
                        {item.title}
                      </p>
                    </div>
                  </div>
                ),
              )}
            </div>
          </div>
        </section>
      </>
    );
  }
}

export default Category;
