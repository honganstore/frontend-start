import { put, takeLatest } from 'redux-saga/effects';
import mockCategory from './components/Category/mockCategory/mockCategory.json';
import * as actions from './actions';
import * as constants from './constants';
// import * as selectors from './selectors';
// Individual exports for testing

function* onGetJobList() {
  try {
    //! do something
  } catch (err) {
    yield put(actions.getJobListFailed(err));
  }
}

function* onGetCategory() {
  // console.log('Go saga onGetCategory');
  // console.log(mockCategory);
  try {
    // console.log('onGetCategory successfully in saga');
    yield put(actions.getCategorySuccess(mockCategory.data));
  } catch (err) {
    yield put(actions.getCategoryFailed(err));
  }
}

export default function* homeSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(constants.GET_JOB_LIST, onGetJobList);
  yield takeLatest(constants.GET_CATEGORY, onGetCategory);
}
