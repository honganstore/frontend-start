/*
 *
 * Home reducer
 *
 */
import produce from 'immer';
import * as constants from './constants';

export const initialState = {
  jobList: [],
  error: null,
  categories: [],
};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>
  produce(state, (newState) => {
    switch (action.type) {
      case constants.GET_JOB_LIST:
        break;
      case constants.GET_JOB_LIST_SUCCESS:
        // newState.campaignDetailOverview = action.payload;
        newState.jobList = action.payload;
        break;
      case constants.GET_JOB_LIST_FAILED:
        // newState.error = action.error;
        break;

      case constants.GET_CATEGORY:
        break;
      case constants.GET_CATEGORY_SUCCESS:
        newState.categories = action.payload;
        break;
      case constants.GET_CATEGORY_FAILED:
        // newState.error = action.error;
        break;
      default:
        break;
    }
  });

export default homeReducer;
