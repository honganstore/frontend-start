/*
 *
 * Home constants
 *
 */

export const GET_JOB_LIST = 'Portal/Home/GET_JOB_LIST';
export const GET_JOB_LIST_SUCCESS = 'Portal/Home/GET_JOB_LIST_SUCCESS';
export const GET_JOB_LIST_FAILED = 'Portal/Home/GET_JOB_LIST_FAILED';

export const GET_CATEGORY = 'w/Home/GET_CATEGORY';
export const GET_CATEGORY_SUCCESS = 'w/Home/GET_CATEGORY_SUCCESS';
export const GET_CATEGORY_FAILED = 'w/Home/GET_CATEGORY__FAILED';
