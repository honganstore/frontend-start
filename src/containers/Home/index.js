/**
 *
 * Home
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import theme from '../../utils/theme';
import * as actions from './actions';
import * as globalActions from '../App/actions';

const Container = styled.div`
  // background-color: ${theme.ncpPrimary};
`;

const Category = React.lazy(() => import('./components/Category'));

const OverlapJoblistContainer = styled.div`
  max-width: 1200px;
  height: 400px;
  margin: 0 auto;
  background: white;
`;

class Home extends React.Component {
  componentDidMount() {
    this.props.dispatch(actions.getCategory());
    this.props.dispatch(globalActions.getAccountInfo());
  }

  render() {
    // console.log(this.props);
    const { categories } = this.props;
    return (
      <Container>
        <Helmet>
          <title>Home</title>
          <meta name="description" content="Description of Home" />
        </Helmet>
        <OverlapJoblistContainer>
          <Category categories={categories} />
        </OverlapJoblistContainer>
      </Container>
    );
  }
}

Home.propTypes = {
  dispatch: PropTypes.func.isRequired,
  categories: PropTypes.any,
};

export default Home;
