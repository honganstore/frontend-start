/* eslint-disable react/prefer-stateless-function */
/**
 *
 * Product
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

import * as productActions from '../../actions';
import * as globalActions from '../../../App/actions';

import ProductList from '../../components/ProductList/Injectable';

export class AllProducts extends React.Component {
  componentDidMount() {
    this.props.dispatch(globalActions.getAccountInfo());
    this.props.dispatch(productActions.getAllProduct());
    this.props.dispatch(productActions.getClientVariantAll());
  }

  render() {
    // console.log('All Product props:', this.props);
    const { allProduct, total, clientVariantAll } = this.props;
    return (
      <div>
        <Helmet>
          <title>AllProducts</title>
          <meta name="description" content="Description of AllProducts" />
        </Helmet>
        <ProductList
          data={{
            allProduct,
            total,
            clientVariantAll,
          }}
        />
      </div>
    );
  }
}

AllProducts.propTypes = {
  dispatch: PropTypes.func,
  allProduct: PropTypes.any,
  total: PropTypes.any,
  clientVariantAll: PropTypes.any,
};

export default AllProducts;
