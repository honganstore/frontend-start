import { compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import Product from '.';
import {
  makeSelectAllProduct,
  makeSelectTotal,
  makeSelectClientVariantAll,
} from '../../selectors';

const mapStateToProps = createStructuredSelector({
  allProduct: makeSelectAllProduct(),
  total: makeSelectTotal(),
  clientVariantAll: makeSelectClientVariantAll(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(Product);
