import { compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import ProductDetails from '.';
import { makeSelectSingleProduct } from '../../selectors';

const mapStateToProps = createStructuredSelector({
  singleProduct: makeSelectSingleProduct(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(ProductDetails);
