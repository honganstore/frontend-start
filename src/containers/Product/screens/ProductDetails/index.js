/* eslint-disable react/no-unused-state */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable react/jsx-props-no-spreading */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as actions from '../../actions';
import * as globalActions from '../../../App/actions';
import styles from '../../../../assets/details/detailsrender.module.css';

import MainSection from '../../components/Details/MainSection/Injectable';

class Details extends Component {
  constructor(props) {
    super(props);
    this.state = {
      params: '',
    };
  }

  componentDidMount() {
    this.props.dispatch(globalActions.getAccountInfo());
    this.props.dispatch(
      actions.getSingleProduct(this.props.match.params.cloth_id),
    );
    this.props.dispatch(
      actions.getColorOfSize(this.props.match.params.cloth_id),
    );
    // this.props.dispatch(actions.getAllProduct());
    this.setState({ params: this.props.match.params.cloth_id });
  }

  getSnapshotBeforeUpdate(prevProps) {
    if (prevProps.match.params.cloth_id !== this.props.match.params.cloth_id) {
      return true;
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (snapshot !== null && snapshot === true) {
      this.props.dispatch(
        actions.getSingleProduct(this.props.match.params.cloth_id),
      );
      this.props.dispatch(
        actions.getColorOfSize(this.props.match.params.cloth_id),
      );
    }
  }

  render() {
    // console.log('Details list props:', this.props);
    const { singleProduct } = this.props;
    return (
      <main>
        <div className={`${styles.detailsContainer} marginToHeader`}>
          <MainSection productDetails={singleProduct} />
        </div>
      </main>
    );
  }
}

Details.propTypes = {
  dispatch: PropTypes.func,
  match: PropTypes.any,
  singleProduct: PropTypes.any,
};

export default Details;
