/*
 *
 * Product actions
 *
 */

import * as constants from './constants';

export function getAllProduct() {
  return {
    type: constants.GET_ALL_PRODUCT,
  };
}

export function getAllProductSuccess(payload) {
  return {
    type: constants.GET_ALL_PRODUCT_SUCCESS,
    payload,
  };
}

export function getAllProductFailed(error) {
  return {
    type: constants.GET_ALL_PRODUCT_FAILED,
    error,
  };
}

export function getClientVariantAll() {
  return {
    type: constants.GET_CLIENT_VARIANT_ALL,
  };
}

export function getClientVariantAllSuccess(payload) {
  return {
    type: constants.GET_CLIENT_VARIANT_ALL_SUCCESS,
    payload,
  };
}

export function getClientVariantAllFailed(error) {
  return {
    type: constants.GET_CLIENT_VARIANT_ALL_FAILED,
    error,
  };
}

export function getSingleProduct(clothId) {
  return {
    type: constants.GET_SINGLE_PRODDUCT,
    clothId,
  };
}

export function getSingleProductSuccess(payload) {
  return {
    type: constants.GET_SINGLE_PRODDUCT_SUCCESS,
    payload,
  };
}

export function getSingleProductFailed(error) {
  return {
    type: constants.GET_SINGLE_PRODDUCT_FAILED,
    error,
  };
}

export function getColorOfSize(clothId) {
  return {
    type: constants.GET_COLOR_OF_SIZE,
    clothId,
  };
}

export function getColorOfSizeSuccess(payload) {
  return {
    type: constants.GET_COLOR_OF_SIZE_SUCCESS,
    payload,
  };
}

export function getColorOfSizeFailed(error) {
  return {
    type: constants.GET_COLOR_OF_SIZE_FAILED,
    error,
  };
}

export function changeColorOfOneSize(request) {
  return {
    type: constants.CHANGE_COLOR_OF_ONE_SIZE,
    request,
  };
}

export function changeColorOfOneSizeSuccess(payload) {
  return {
    type: constants.CHANGE_COLOR_OF_ONE_SIZE_SUCCESS,
    payload,
  };
}

export function changeColorOfOneSizeFailed(error) {
  return {
    type: constants.CHANGE_COLOR_OF_ONE_SIZE_FAILED,
    error,
  };
}

export function addToCart(request) {
  return {
    type: constants.ADD_TO_CART,
    request,
  };
}

export function addToCartSuccess(payload) {
  return {
    type: constants.ADD_TO_CART_SUCCESS,
    payload,
  };
}

export function addToCartFailed(error) {
  return {
    type: constants.ADD_TO_CART_FAILED,
    error,
  };
}

export function deleteFromCart(variantId) {
  return {
    type: constants.DELETE_FROM_CART,
    variantId,
  };
}

export function deleteFromCartSuccess(payload) {
  return {
    type: constants.DELETE_FROM_CART_SUCCESS,
    payload,
  };
}

export function deleteFromCartFailed(error) {
  return {
    type: constants.DELETE_FROM_CART_FAILED,
    error,
  };
}

export function paymentCartSuccess(status) {
  return {
    type: constants.PAYMENT_CART_SUCCESS,
    status,
  };
}

export function paymentCartFailed(error) {
  return {
    type: constants.PAYMENT_CART_FAILED,
    error,
  };
}
