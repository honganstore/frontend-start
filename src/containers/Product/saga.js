import { takeLatest, put, call } from 'redux-saga/effects';
import _ from 'lodash';
import { toast } from 'react-toastify';
import * as constants from './constants';
import request from '../../utils/request';
import config from '../../global-config';
import * as actions from './actions';

function* onGetAllProduct() {
  // console.log('go saga');
  const requestURL = `${config.apiUrl}/cloth/all`;
  try {
    const res = yield call(request, requestURL);
    // console.log(res);
    yield put(actions.getAllProductSuccess(res));
  } catch (error) {
    yield put(actions.getAllProductFailed(error));
  }
}

function* onGetClientVariantAll() {
  // console.log('go saga');
  const requestURL = `${config.apiUrl}/variant/join/full/all`;
  try {
    const res = yield call(request, requestURL);
    // console.log(res);
    yield put(actions.getClientVariantAllSuccess(res.result));
  } catch (error) {
    yield put(actions.getClientVariantAllFailed(error));
  }
}

function* onGetSingleProduct(action) {
  // console.log('go saga');
  const requestURL = `${config.apiUrl}/variant/join/full/single/${action.clothId}`;
  try {
    const res = yield call(request, requestURL);
    const result = _.get(res, 'result', '');
    yield put(actions.getSingleProductSuccess(result));
  } catch (error) {
    yield put(actions.getSingleProductFailed(error));
  }
}

function* onGetColorOfSize(action) {
  // console.log('go saga');
  // console.log(action);
  const requestURL = `${config.apiUrl}/variant/join/size-color/${action.clothId}`;
  try {
    const res = yield call(request, requestURL);
    // console.log('size color single:', res);
    yield put(actions.getColorOfSizeSuccess(res));
  } catch (error) {
    yield put(actions.getColorOfSizeFailed(error));
  }
}

function* onChangeColorOfOneSize(action) {
  // console.log('go saga');
  // console.log(action);
  try {
    yield put(actions.changeColorOfOneSizeSuccess(action.request));
  } catch (error) {
    yield put(actions.changeColorOfOneSizeFailed(error));
  }
}

function* onAddToCart(action) {
  // console.log(action);
  try {
    if (action.request.color !== '' && action.request.size !== '') {
      yield put(actions.addToCartSuccess(action.request));
    } else {
      toast.warn('Bạn chưa chọn size hoặc màu của sản phẩm này.');
    }
  } catch (error) {
    yield put(actions.addToCartFailed(error));
  }
}

function* onDeleteFromCart(action) {
  // console.log(action);
  try {
    yield put(actions.deleteFromCartSuccess(action.variantId));
  } catch (error) {
    yield put(actions.deleteFromCartFailed(error));
  }
}

// Individual exports for testing
export default function* modalsSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(constants.GET_ALL_PRODUCT, onGetAllProduct);
  yield takeLatest(constants.GET_CLIENT_VARIANT_ALL, onGetClientVariantAll);
  yield takeLatest(constants.GET_SINGLE_PRODDUCT, onGetSingleProduct);
  yield takeLatest(constants.GET_COLOR_OF_SIZE, onGetColorOfSize);
  yield takeLatest(constants.CHANGE_COLOR_OF_ONE_SIZE, onChangeColorOfOneSize);
  yield takeLatest(constants.ADD_TO_CART, onAddToCart);
  yield takeLatest(constants.DELETE_FROM_CART, onDeleteFromCart);
}
