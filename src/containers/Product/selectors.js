import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the Product state domain
 */

const selectProductDomain = (state) => state.product || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Product
 */

const makeSelectProduct = () =>
  createSelector(selectProductDomain, (substate) => substate);

export const makeSelectAllProduct = () =>
  createSelector(selectProductDomain, (substate) => substate.allProduct);

export const makeSelectTotal = () =>
  createSelector(selectProductDomain, (substate) => substate.total);

export const makeSelectSingleProduct = () =>
  createSelector(selectProductDomain, (substate) => substate.singleProduct);

export const makeSelectFilterSize = () =>
  createSelector(selectProductDomain, (substate) => substate.filterSize);

export const makeSelectColorOfSize = () =>
  createSelector(selectProductDomain, (substate) => substate.colorOfSize);

export const makeSelectCart = () =>
  createSelector(selectProductDomain, (substate) => substate.cartItem);

export const makeSelectClientVariantAll = () =>
  createSelector(selectProductDomain, (substate) => substate.clientVariantAll);

export default makeSelectProduct;
export { selectProductDomain };
