/* eslint-disable camelcase */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable react/button-has-type */
import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { FiShoppingCart } from 'react-icons/fi';
import { BsSearch } from 'react-icons/bs';
import styles from '../../../../assets/product/products.module.css';

class ProductItem extends React.Component {
  handleCartClick = (event) => {
    event.preventDefault();
    // console.log('ID add:', this.props.id);
    this.props.onAddToCart(this.props.cloth_id);
    // console.log(this.props.cloth_id);
  };

  render() {
    const { img_1, cloth_name, price, cloth_id } = this.props;
    console.log('Props Product Item:', this.props);
    return (
      <NavLink
        key={cloth_id}
        to={`/product/details/${cloth_id}`}
        activeClassName="active"
      >
        <div
          className={styles.product}
          id={cloth_id}
          onClick={this.handleDetailsClick}
        >
          <div className={styles.overflow}>
            <div className={styles.productImg}>
              <img src={img_1} alt="Product" />
              <button
                className={styles.btnAddToCart}
                onClick={this.handleCartClick}
              >
                <FiShoppingCart />
              </button>
              <button className={styles.btnSearchDetail}>
                <BsSearch />
              </button>
            </div>
          </div>
          <div className={styles.title}>{cloth_name}</div>
          <div className={styles.price}>{price}</div>
        </div>
      </NavLink>
    );
  }
}

ProductItem.propTypes = {
  onAddToCart: PropTypes.any,
  cloth_id: PropTypes.any,
  img_1: PropTypes.any,
  cloth_name: PropTypes.any,
  price: PropTypes.any,
};

export default ProductItem;
