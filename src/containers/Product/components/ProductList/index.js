/* eslint-disable react/jsx-props-no-spreading */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import styles from '../../../../assets/product/products.module.css';
import stylesFrame from '../../../../assets/product/product.module.css';

import ProductItem from '../ProductItem';

import * as actions from '../../actions';

class ProductList extends Component {
  handleAddToCart = (id) => {
    const product = this.props.data.clientVariantAll.find(
      (item) => id === item.cloth_id,
    );
    if (product) {
      this.props.dispatch(actions.addToCart(product));
    }
  };

  render() {
    console.log('Product list props:', this.props);
    // console.log('product fetch', products);
    return (
      <section className={stylesFrame.product}>
        <div className={stylesFrame.productFrame}>
          <div className={styles.productFrame}>
            {_.get(this.props, 'data.allProduct', '').map((item) => (
              <ProductItem {...item} onAddToCart={this.handleAddToCart} />
            ))}
          </div>
        </div>
      </section>
    );
  }
}

ProductList.propTypes = {
  dispatch: PropTypes.func,
  allProduct: PropTypes.any,
  data: PropTypes.any,
};

export default ProductList;
