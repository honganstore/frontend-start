/* eslint-disable react/prefer-stateless-function */
/* eslint-disable react/no-unescaped-entities */
import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from '../../../../../assets/details/detailsrender.module.css';

export default class DirectLink extends React.Component {
  render() {
    return (
      <div className={styles.directLink}>
        <NavLink href="#" id="firstDirect">
          Trang chủ
        </NavLink>
        <span>></span>
        <NavLink href="#" id="secondDirect">
          Đầm
        </NavLink>{' '}
        <span>></span>
        <NavLink href="#" id="rdDirect">
          Đầm Dáng Ôm
        </NavLink>
      </div>
    );
  }
}
