/* eslint-disable react/no-did-update-set-state */
/* eslint-disable react/no-unused-state */
import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import styles from '../../../../../assets/details/detailsrightsection.module.css';
import '../../../../../index.css';

import * as actions from '../../../actions';

export default class DetailsRightSection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rdColor: '',
      rdSize: '',
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  getSnapshotBeforeUpdate(prevProps) {
    if (prevProps.colorOfSize.size !== this.props.colorOfSize.size) {
      return true;
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (snapshot !== null && snapshot === true) {
      this.setState({ rdSize: this.props.colorOfSize.size });
      this.setState({ rdColor: this.props.colorOfSize.colors[0] });
    }
  }

  handleCheckColor = (color) => {
    if (!color || color === '') {
      return true;
    }
    return null;
  };

  handleChange(event) {
    const { target } = event;
    const { name } = target;
    const { value } = target;
    this.setState({
      [name]: value,
    });
    if (name === 'rdSize') {
      this.props.dispatch(
        actions.changeColorOfOneSize({
          size: value,
        }),
      );
    }
  }

  handleSubmit(event) {
    event.preventDefault();
  }

  handleCartClick = (event) => {
    event.preventDefault();
    this.props.onAddToCart({
      ...this.props,
      cloth_id: this.props.cloth_id,
      color: this.state.rdColor,
      size: this.state.rdSize,
    });
  };

  render() {
    // console.log('state right section:', this.state);
    const { filterSize, colorOfSize } = this.props;
    // console.log(colorOfSize);
    return (
      <div className={styles.detailsOption}>
        <form className={styles.formToCart}>
          Màu sắc: <br />
          <div className={styles.radio}>
            {_.get(colorOfSize, 'colors', []).map((item) => (
              <label className={styles.formLabel}>
                <input
                  type="radio"
                  name="rdColor"
                  value={item}
                  onChange={this.handleChange}
                />
                <span className={`${styles.checkMark} ${item}`}></span>
              </label>
            ))}
          </div>
          {this.handleCheckColor(this.state.rdColor) ? (
            <p>Vui lòng chọn màu</p>
          ) : null}
          Kích cỡ: <br />
          <div className={styles.radio}>
            {filterSize.map((item) => (
              <label className={styles.formLabel}>
                <input
                  type="radio"
                  name="rdSize"
                  value={item.size}
                  onChange={this.handleChange}
                />
                <span className={`${styles.checkMark}`}>{item.size}</span>
              </label>
            ))}
          </div>
          {this.handleCheckColor(this.state.rdSize) ? (
            <p>Vui lòng chọn size</p>
          ) : null}
          <p className={styles.noteStore}>
            Có sẵn tất cả kích cỡ. Sắp hết hàng
          </p>
          <button
            type="button"
            // onSubmit={this.handleSubmit}
            onClick={this.handleCartClick}
            className={styles.btnSubmit}
          >
            Cho vào giỏ hàng
          </button>
        </form>
        <div className={styles.share}>
          <p>Chia sẽ</p>
        </div>
        <div className={styles.question}>
          <p>BẠN CÓ THẮC MẮC?</p>
          <p>Hãy truy cập trang HỎI ĐÁP</p>
          <p>
            <u>Thời gian làm việc:</u>
          </p>
          <p>Điện thoại: 8:00 - 18:00</p>
          <p> (Thứ 2 đến CN)</p>
          <p>Ngày Lễ: 9:00 - 18:00 </p>
        </div>
      </div>
    );
  }
}

DetailsRightSection.propTypes = {
  dispatch: PropTypes.func,
  onAddToCart: PropTypes.func,
  cloth_id: PropTypes.any,
  filterSize: PropTypes.any,
  colorOfSize: PropTypes.any,
};
