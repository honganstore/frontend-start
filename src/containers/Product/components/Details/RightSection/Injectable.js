import { compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import DetailsRightSection from '.';

import {
  makeSelectColorOfSize,
  makeSelectFilterSize,
} from '../../../selectors';

const mapStateToProps = createStructuredSelector({
  colorOfSize: makeSelectColorOfSize(),
  filterSize: makeSelectFilterSize(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(DetailsRightSection);
