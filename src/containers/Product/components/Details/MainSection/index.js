/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import styles from '../../../../../assets/details/detailsleftsection.module.css';

import DetailsRightSection from '../RightSection/Injectable';

import * as actions from '../../../actions';

class MainSection extends React.Component {
  handleAddToCart = (item) => {
    // console.log(item);
    if (this.props.productDetails.cloth_id === item.cloth_id) {
      this.props.dispatch(actions.addToCart(item));
      // console.log('success');
    }
  };

  render() {
    const { productDetails } = this.props;
    return (
      <section className={styles.detailsContent}>
        <div className={styles.info} id={`${productDetails.cloth_id}`}>
          <h3>{productDetails.cloht_name}</h3>
          <p>
            {new Intl.NumberFormat('de-DE').format(productDetails.minPrice)}{' '}
            <span>VNĐ</span> -
            {new Intl.NumberFormat('de-DE').format(productDetails.maxPrice)}{' '}
            <span>VNĐ</span>
          </p>

          <div className={styles.describe}>{productDetails.description}</div>
          <div className={styles.note}>
            <p>Miễn phí giao hàng từ 449.000 đ</p>
            <p>30 NGÀY ĐỔI TRẢ (*)</p>
            <p>Thanh toán khi nhận hàng </p>
            <div className={`${styles.note} ${styles.subNote}`}>
              <span>Sẽ có tại nhà bạn</span>
              <span>trong 3-5 ngày làm việc</span>
            </div>
          </div>
        </div>
        <div className={styles.detailsImage}>
          <img
            className={styles.mainImg}
            src={productDetails.img_1}
            alt="Product"
          />
          {_.get(productDetails, 'img_2', '') !== '' ? (
            <img
              className={styles.subImg}
              src={productDetails.img_2}
              alt="Product Sub"
            />
          ) : null}

          {_.get(productDetails, 'img_3', '') !== '' ? (
            <img
              className={styles.subImg}
              src={productDetails.img_2}
              alt="Product Sub"
            />
          ) : null}
        </div>
        <DetailsRightSection
          onAddToCart={this.handleAddToCart}
          {...productDetails}
        />
      </section>
    );
  }
}

MainSection.propTypes = {
  dispatch: PropTypes.any,
  productDetails: PropTypes.any,
};

export default MainSection;
