/**
 *
 * Product
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

import { Redirect, Switch, Route } from 'react-router-dom';

import AllProducts from './screens/All/Loadable';

import ProductDetails from './screens/ProductDetails/Loadable';
export function Product(props) {
  return (
    <>
      <Switch>
        <Route exact path={`${props.match.path}`} component={AllProducts} />
        <Route
          exact
          path={`${props.match.path}/details/:cloth_id`}
          component={ProductDetails}
        />
        <Redirect to="/" />
      </Switch>
    </>
  );
}

Product.propTypes = {
  match: PropTypes.any,
};

export default Product;
