/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
/* eslint-disable no-case-declarations */
/* eslint-disable prefer-destructuring */
/*
 *
 * Product reducer
 *
 */
import produce from 'immer';
import * as constants from './constants';

export const initialState = {
  allProduct: [],
  total: 0,
  singleProduct: {},
  variantAll: [],
  clientVariantAll: [],
  cartItem: JSON.parse(localStorage.getItem('cartItem')) || {
    cartItemList: [],
    totalPrice: 0,
  },
  filterSize: [],
  colorOfSize: {},
};

/* eslint-disable default-case, no-param-reassign */
const productReducer = (state = initialState, action) =>
  produce(state, (newState) => {
    switch (action.type) {
      case constants.GET_ALL_PRODUCT:
        break;

      case constants.GET_ALL_PRODUCT_SUCCESS:
        newState.allProduct = action.payload.result;
        newState.total = action.payload.total;
        break;

      case constants.GET_ALL_PRODUCT_FAILED:
        break;

      case constants.GET_CLIENT_VARIANT_ALL:
        break;

      case constants.GET_CLIENT_VARIANT_ALL_SUCCESS:
        newState.clientVariantAll = action.payload;
        break;

      case constants.GET_CLIENT_VARIANT_ALL_FAILED:
        break;

      case constants.GET_SINGLE_PRODDUCT:
        break;

      case constants.GET_SINGLE_PRODDUCT_SUCCESS:
        newState.singleProduct = action.payload[0];
        const arrayPrice = action.payload.map((item) => item.price);
        const minPrice = Math.min(...arrayPrice);
        const maxPrice = Math.max(...arrayPrice);
        newState.singleProduct.minPrice = minPrice;
        newState.singleProduct.maxPrice = maxPrice;
        break;

      case constants.GET_SINGLE_PRODDUCT_FAILED:
        break;

      case constants.GET_COLOR_OF_SIZE:
        break;

      case constants.GET_COLOR_OF_SIZE_SUCCESS:
        newState.variantAll = action.payload.result;
        newState.filterSize = action.payload.filterSize;
        newState.colorOfSize = action.payload.filterSize[0];
        break;

      case constants.GET_COLOR_OF_SIZE_FAILED:
        break;

      case constants.CHANGE_COLOR_OF_ONE_SIZE:
        break;

      case constants.CHANGE_COLOR_OF_ONE_SIZE_SUCCESS:
        const filterSizeItem = state.filterSize.filter((item) => {
          if (item.size === action.payload.size) return item;
        });
        newState.colorOfSize = filterSizeItem[0];
        break;

      case constants.CHANGE_COLOR_OF_ONE_SIZE_FAILED:
        break;

      case constants.ADD_TO_CART:
        break;

      case constants.ADD_TO_CART_SUCCESS:
        // console.log(state.variantAll);
        // console.log(action);
        let updateCart;
        // Tìm kiếm variant trùng với list variant all
        const cartAddItem = state.variantAll.find((variant) => {
          if (
            variant.size === action.payload.size &&
            variant.cloth_id === action.payload.cloth_id &&
            variant.color === action.payload.color
          ) {
            return variant;
          }
        });
        // console.log('cartAddItem', cartAddItem);

        // NẾU ACTION ĐƯỢC GỌI TỪ DETAIL PAGE
        if (cartAddItem !== undefined) {
          // Kiểm tra variant này đã có được thêm vào giỏ hàng chưa

          const checked = state.cartItem.cartItemList.find(
            (item) => item.variant_id === cartAddItem.variant_id,
          );
          // Nếu đã có thì giữ nguyên mọi tham số, chỉ thay đổi tổng tiền và số lượng
          if (checked) {
            updateCart = state.cartItem.cartItemList.map((item) => {
              if (item.variant_id === checked.variant_id) {
                // console.log(item);
                // console.log('mach');
                return {
                  ...item,
                  quantity: item.quantity + 1,
                  totalPrice: (item.quantity + 1) * item.price,
                };
              }
              return item;
            });
            // Nếu chưa thì giữ nguyên mảng các cartItem cũ và nối thêm cartItem mới vào
          } else {
            updateCart = [
              ...state.cartItem.cartItemList,
              {
                ...action.payload,
                ...cartAddItem,
                totalPrice: cartAddItem.price,
                quantity: 1,
              },
            ];
          }
          // console.log('updateCart', updateCart);

          // NẾU ACTION ĐƯỢC GỌI TỪ PRODUCT PAGE
        } else {
          const checked = state.cartItem.cartItemList.find(
            (item) => item.variant_id === action.payload.variant_id,
          );

          if (checked) {
            updateCart = state.cartItem.cartItemList.map((item) => {
              if (item.variant_id === action.payload.variant_id) {
                // console.log(item);
                // console.log('mach');
                return {
                  ...item,
                  quantity: item.quantity + 1,
                  totalPrice: (item.quantity + 1) * item.price,
                };
              }
              return item;
            });
            // Nếu chưa thì giữ nguyên mảng các cartItem cũ và nối thêm cartItem mới vào
          } else {
            updateCart = [
              ...state.cartItem.cartItemList,
              {
                ...action.payload,
                totalPrice: action.payload.price,
                quantity: 1,
              },
            ];
          }
        }

        newState.cartItem.cartItemList = updateCart;
        newState.cartItem.count = calculateTotalCount(updateCart);
        newState.cartItem.totalPrice = calculateTotal(updateCart);

        // CartItem to local storage
        const cartItem = {
          cartItemList: newState.cartItem.cartItemList,
          count: newState.cartItem.count,
          totalPrice: newState.cartItem.totalPrice,
        };

        localStorage.setItem('cartItem', JSON.stringify(cartItem));

        break;

      case constants.ADD_TO_CART_FAILED:
        break;

      case constants.DELETE_FROM_CART:
        break;

      case constants.DELETE_FROM_CART_SUCCESS:
        // console.log(action);
        // console.log(state.cartItem.cartItemList);
        const filterOutOfCartItem = state.cartItem.cartItemList.filter(
          (item) => item.variant_id !== action.payload,
        );
        // console.log('filterOutOfCartItem', filterOutOfCartItem);

        newState.cartItem.cartItemList = filterOutOfCartItem;
        newState.cartItem.count = calculateTotalCount(filterOutOfCartItem);
        newState.cartItem.totalPrice = calculateTotal(filterOutOfCartItem);

        const cartItemAfterDetelte = {
          cartItemList: newState.cartItem.cartItemList,
          count: newState.cartItem.count,
          totalPrice: newState.cartItem.totalPrice,
        };

        // console.log("cartItemAfterDetelte", cartItemAfterDetelte);
        localStorage.setItem('cartItem', JSON.stringify(cartItemAfterDetelte));

        break;

      case constants.DELETE_FROM_CART_FAILED:
        break;

      case constants.PAYMENT_CART_SUCCESS:
        newState.cartItem.cartItemList = [];
        newState.cartItem.count = 0;
        newState.cartItem.totalPrice = 0;
        const cartItemAfterPayment = {
          cartItemList: newState.cartItem.cartItemList,
          count: newState.cartItem.count,
          totalPrice: newState.cartItem.totalPrice,
        };
        localStorage.setItem('cartItem', JSON.stringify(cartItemAfterPayment));
        break;
      case constants.PAYMENT_CART_FAILED:
        break;
      default:
        break;
    }
  });

// Total Price All Cloth = 0 + price * quantity;
function calculateTotal(items) {
  return items.reduce((sum, item) => sum + item.price * item.quantity, 0);
}
// Total Count All Cloth = 0 + quantity;
function calculateTotalCount(items) {
  return items.reduce((sum, item) => sum + item.quantity, 0);
}

export default productReducer;
