/*
 *
 * Product reducer
 *
 */
import produce from 'immer';
import * as constants from './constants';

export const initialState = {};

/* eslint-disable default-case, no-param-reassign */
const checkoutReducer = (state = initialState, action) =>
  produce(state, (/* draft */) => {
    switch (action.type) {
      case constants.PAYMENT_CART:
        break;

      default:
        break;
    }
  });

export default checkoutReducer;
