/* eslint-disable react/prop-types */
/* eslint-disable react/prefer-stateless-function */
/**
 *
 * Checkout
 *
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { Button } from 'reactstrap';
import styled from 'styled-components';
import CheckoutList from './components/CheckoutList';
import FormInput from '../../components/Form/FormInput';
import * as actions from './actions';
import * as globalActions from '../App/actions';
import * as modalsAction from '../Modals/actions';

const CheckoutWrapper = styled.main`
  max-width: 1200px;
  margin: 0 auto;
`;

const CheckoutContent = styled.section`
  width: 100%;
  display: flex;
  margin: 0 auto;
`;

const PaymenWrapper = styled.div`
  width: 400px;
  background-color: white;
  display: flex;
  flex-direction: column;
`;

const StyledButton = styled(Button)`
  margin: 22px 0;
  width: 100%;
  height: 60px;
  background-color: white;
  font-size: 18px;
  color: black;
  &:hover,
  &:active,
  &:focus {
    box-shadow: unset;
    background-color: black;
    color: white;
  }
`;

const CheckoutTitle = styled.h1`
  width: 100%;
  font-size: 40px;
  padding: 32px;
  text-align: center;
`;

const TextWrapper = styled.div`
  width: 100%;
`;

const Title = styled.p`
  cursor: context-menu;
  font-size: 16px;
  font-weight: bold;
  text-transform: uppercase;
  text-align: left;
  margin: 0;
`;

const TextValue = styled(Title)`
  cursor: context-menu;
  border: 0;
  font-weight: normal;
  text-transform: unset;
  border-bottom: 1px solid;
  padding: 6px 12px;
`;

const PaymentWrap = styled.div`
  display: flex;
  flex-direction: column;
`;
export function Checkout({ dispatch, cartItem, isLogin, currentUser }) {
  const [address, setAddress] = useState('');

  useEffect(() => {
    // console.log('Re-useEffect');
    dispatch(globalActions.getAccountInfo());
    setAddress(currentUser.address);
  }, [currentUser.address]);

  const handlePayment = () => {
    if (!isLogin) {
      dispatch(modalsAction.showLoginModals(true));
    }
    if (isLogin && address !== '') {
      dispatch(
        actions.paymentCart({
          cartItem: cartItem.cartItemList,
          address,
        }),
      );
    }
  };

  const handleChange = (event) => {
    const { target } = event;
    const { value } = target;
    setAddress(value);
    // console.log(event.target.value);
  };
  return (
    <div>
      <Helmet>
        <title>Checkout</title>
        <meta name="description" content="Description of Checkout" />
      </Helmet>
      <CheckoutWrapper>
        <CheckoutTitle>SHOPPING CART</CheckoutTitle>
        <CheckoutContent>
          <CheckoutList cartItem={cartItem} />
          <PaymentWrap>
            {!isLogin ? null : (
              <PaymenWrapper>
                <TextWrapper>
                  <Title>Full name</Title>
                  <TextValue>{`${currentUser.first_name} ${currentUser.last_name}`}</TextValue>
                </TextWrapper>
                <TextWrapper>
                  <Title>Email</Title>
                  <TextValue>{currentUser.email}</TextValue>
                </TextWrapper>
                <TextWrapper>
                  <Title>Phone</Title>
                  <TextValue>{currentUser.phone}</TextValue>
                </TextWrapper>
                <FormInput
                  title="Address"
                  type="text"
                  name="address"
                  value={address}
                  onChange={handleChange}
                />
                {address === '' && (
                  <p style={{ color: 'red' }}>
                    Vui lòng nhập địa chỉ giao hàng
                  </p>
                )}
              </PaymenWrapper>
            )}
            <StyledButton onClick={handlePayment}>Mua hàng</StyledButton>
          </PaymentWrap>
        </CheckoutContent>
      </CheckoutWrapper>
    </div>
  );
}

Checkout.propTypes = {
  cartItem: PropTypes.any,
  dispatch: PropTypes.func,
  isLogin: PropTypes.any,
};

export default Checkout;
