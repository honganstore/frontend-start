/* eslint-disable camelcase */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Wrapper = styled.article`
  width: 100%;
  height: 208px;
  padding-bottom: 32px;
  display: flex;
`;

const ItemBgWrapper = styled.div`
  width: 20%;
  height: 100%;
  margin: 0 15px 0 0;
  background-image: url(${(props) => props.bgImage});
  background-repeat: no-repeat;
  background-position: center;
  background-size: contain;
`;

const ItemContent = styled.div`
  width: 80%;
  display: flex;
  flex-direction: column;
  .item-head {
    display: flex;
    flex-direction: column;
    padding-top: 20px;
    h3 {
      font-size: 13px;
      color: black;
    }
    .price {
      font-size: 12px;
      color: black;
    }
  }
  .item-info {
    display: flex;
    margin: 30px 0 0 0;
    .item-info--content {
      display: flex;
      margin: 0 15px 0 0;
      .item-info__value {
        margin: 0 20px 0 0;
      }
    }
  }
`;

export default class CheckoutItem extends React.PureComponent {
  render() {
    const {
      variant_id,
      cate_name,
      brand,
      totalPrice,
      quantity,
      price,
      cloth_id,
      cloth_name,
      img_1,
      color,
      size,
    } = this.props;
    return (
      <Wrapper>
        <ItemBgWrapper bgImage={img_1} />
        <ItemContent>
          <div className="item-head">
            <h3>{cloth_name}</h3>
            <div className="price">
              {new Intl.NumberFormat('de-DE').format(price)} VNĐ x {quantity} ={' '}
              {new Intl.NumberFormat('de-DE').format(totalPrice)} VNĐ
            </div>
          </div>
          <div className="item-info">
            <div className="item-info--content">
              <div className="item-info__value">Bộ sưu tập: </div>
              <div className="item-info__value">{cate_name}</div>
            </div>
            <div className="item-info--content">
              <div className="item-info__value">Thương hiệu: </div>
              <div className="item-info__value">{brand}</div>
            </div>
          </div>
          <div className="item-info">
            <div className="item-info--content">
              <div className="item-info__value">Variant: </div>
              <div className="item-info__value">{variant_id}</div>
            </div>
            <div className="item-info--content">
              <div className="item-info__value">Mã SP: </div>
              <div className="item-info__value">{cloth_id}</div>
            </div>
            <div className="item-info--content">
              <div className="item-info__value">Màu: </div>
              <div className={`item-info__value ${color}`}></div>
            </div>
            <div className="item-info--content">
              <div className="item-info__value">Size: </div>
              <div className="item-info__value">{size} </div>
            </div>
          </div>
        </ItemContent>
      </Wrapper>
    );
  }
}

CheckoutItem.propTypes = {
  variant_id: PropTypes.any,
  cate_name: PropTypes.any,
  brand: PropTypes.any,
  totalPrice: PropTypes.any,
  quantity: PropTypes.any,
  price: PropTypes.any,
  cloth_id: PropTypes.any,
  cloth_name: PropTypes.any,
  img_1: PropTypes.any,
  color: PropTypes.any,
  size: PropTypes.any,
};
