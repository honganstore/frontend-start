/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import styled from 'styled-components';
import CheckoutItem from '../CheckoutItem';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const CheckOutListWrapper = styled.section`
  width: 800px;
  background-color: #faf9f8;
`;

const TotalRow = styled.div`
  display: flex;
  flex-direction: column;
`;

class CheckOutList extends React.PureComponent {
  render() {
    const { cartItem } = this.props;
    // console.log(cartItem);
    return (
      <Wrapper>
        <CheckOutListWrapper>
          {_.get(cartItem, 'cartItemList', []).map((item) => (
            <CheckoutItem {...item} />
          ))}
        </CheckOutListWrapper>
        <TotalRow>
          <div>Tổng số lượng: {cartItem.count}</div>
          <div>
            Tổng tiền:{' '}
            {new Intl.NumberFormat('de-DE', {
              style: 'currency',
              currency: 'VND',
            }).format(cartItem.totalPrice)}
          </div>
        </TotalRow>
      </Wrapper>
    );
  }
}

CheckOutList.propTypes = {
  cartItem: PropTypes.any,
};

export default CheckOutList;
