/*
 *
 * Checkout actions
 *
 */

import * as constants from './constants';

export function paymentCart(request) {
  return {
    type: constants.PAYMENT_CART,
    request,
  };
}
