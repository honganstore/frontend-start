import { takeLatest, put } from 'redux-saga/effects';
// import { call } from 'redux-saga/effects';
import { push } from 'connected-react-router';
import { toast } from 'react-toastify';
// import config from '../../global-config';
// import request from '../../utils/request';
import * as constants from './constants';

import * as actions from '../Product/actions';

function* onPayment(action) {
  // console.log('go saga', action);

  // :))) sorry, Back end wasn't finished
  // But Front end, I could get list Variant of cart items and next step is call API with arr Variant Id, wait Back end respone status, messsage! Done. :3

  const requestData = action.request.cartItem.map((item) => item.variant_id);

  console.log(
    JSON.stringify({ arrVar: requestData, add: action.request.address }),
  );

  // const requestURL = `${config.apiUrl}/payment`;
  try {
    // const res = yield call(request, requestURL, {
    //   method: "POST",
    //   body:
    // })
    yield put(actions.paymentCartSuccess(true));
  } catch (error) {
    yield put(actions.paymentCartFailed(error));
  }
  toast.success('Bạn đã đặt hàng thành công!');
  yield put(push('/'));
}

// Individual exports for testing
export default function* checkoutSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(constants.PAYMENT_CART, onPayment);
}
