import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';
import reducer from './reducer';
import saga from './saga';

import Checkout from '.';

import { makeSelectCart } from '../Product/selectors';
import { makeSelectIsLogin, makeSelectCurrentUser } from '../Home/selectors';

const mapStateToProps = createStructuredSelector({
  cartItem: makeSelectCart(),
  isLogin: makeSelectIsLogin(),
  currentUser: makeSelectCurrentUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'checkout', reducer });
const withSaga = injectSaga({ key: 'checkout', saga });

export default compose(withReducer, withSaga, withConnect)(Checkout);
