/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';
import _ from 'lodash';

const NavWrapper = styled.nav`
  width: 100%;
  font-size: 14px;
  margin-right: 20px;
`;

const NavUl = styled.ul`
  list-style: none;
  background: #e5e5e5;
  border: 1px solid #ddd;
  border-radius: 5px;
  padding-left: 0;
  font-weight: 400;
  line-height: 1.5;
  text-align: left;
  margin-top: 0;
  margin-bottom: 16px;
`;

const NavLi = styled.li`
  border: 1px solid #ddd;
  background-color: ${(props) => (props.isActive ? '#333' : '#e5e5e5')};
  ._link {
    display: block;
    color: ${(props) => (props.isActive ? '#fff' : '#222')};
    text-decoration: none;
    text-align: center;
    padding: 16px;
    &:hover {
      text-decoration: none;
      color: #fff;
    }
  }
  &:first-child {
    border-radius: 5px 5px 0 0;
  }
  &:last-child {
    border-radius: 0 0 5px 5px;
  }
  &:hover {
    background-color: #333;
  }
  &:first-child:hover {
    border-radius: 5px 5px 0 0;
  }
  &:last-child:hover {
    border-radius: 0 0 5px 5px;
  }
`;

export default class AccountNavigation extends React.Component {
  isActiveSidebar = (location, path) => {
    const pathName = _.get(location, 'pathname', '');
    return _.isEqual(pathName, path);
  };

  render() {
    const { location } = this.props;
    // console.log('AccountNav Props:', this.props);
    return (
      <NavWrapper>
        <NavUl>
          <NavLi
            isActive={
              this.isActiveSidebar(location, '/account')
                ? this.isActiveSidebar(location, '/account')
                : undefined
            }
          >
            <NavLink className="_link" to="/account" activeClassName="active">
              Account info
            </NavLink>
          </NavLi>
          <NavLi
            isActive={this.isActiveSidebar(location, '/account/changePassword')}
          >
            <NavLink
              className="_link"
              to="/account/changePassword"
              activeClassName="active"
            >
              Change password
            </NavLink>
          </NavLi>
        </NavUl>
      </NavWrapper>
    );
  }
}

AccountNavigation.propTypes = {
  location: PropTypes.any,
};
