import { takeLatest, put, call } from 'redux-saga/effects';
import { push } from 'connected-react-router';
import { toast } from 'react-toastify';
import _ from 'lodash';
import * as constants from './constants';
import * as actions from './actions';
import config from '../../global-config';
import request from '../../utils/request';
// import ChangePasswordFormToRequest from './mappers/ChangeePasswordFormToRequest';
function* onCheckLogin() {
  try {
    const accessLocal = localStorage.getItem('access_token');
    if (!accessLocal || _.isEmpty(accessLocal) || accessLocal === 'anonymous') {
      yield put(push('/'));
    } else {
      yield put(actions.checkLoginSuccess(true));
      window.scrollTo(0, 0);
    }
  } catch (err) {
    yield put(actions.checkLoginFailed(err));
  }
}

function* onChangePasword(action) {
  // console.log('go saga');
  const requestURl = `${config.apiUrl}/user/update-password/${action.requestData.user_id}`;
  // const requestData = ChangePasswordFormToRequest(action.requestData);
  // console.log('JSON:', JSON.stringify(requestData));
  try {
    const res = yield call(request, requestURl, {
      method: 'PUT',
      body: JSON.stringify({
        oldPassword: action.requestData.password,
        newPassword: action.requestData.newPassword,
      }),
    });
    // console.log(res);
    toast.success('Bạn đã cập nhật mật khẩu thành công');
    yield put(actions.changePasswordSuccess(res));
  } catch (error) {
    toast.error(error.message);
    yield put(actions.changePasswordFailed(error));
  }
}
// Individual exports for testing
export default function* accountSaga() {
  yield takeLatest(constants.CHECK_LOGIN, onCheckLogin);
  yield takeLatest(constants.CHANGE_PASSWORD, onChangePasword);
}
