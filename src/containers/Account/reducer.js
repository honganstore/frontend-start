/*
 *
 * App reducer
 *
 */
import produce from 'immer';
import * as constants from './constants';

export const initialState = {
  changePasswordStatus: 0,
  isLogin: false,
};

/* eslint-disable default-case, no-param-reassign */
const accountReducer = (state = initialState, action) =>
  produce(state, (newState) => {
    switch (action.type) {
      case constants.CHECK_LOGIN:
        break;

      case constants.CHECK_LOGIN_SUCCESS:
        newState.isLogin = action.status;
        break;

      case constants.CHECK_LOGIN_FAILED:
        break;

      case constants.CHANGE_PASSWORD:
        break;

      case constants.CHANGE_PASSWORD_SUCCESS:
        newState.changePasswordStatus = action.payload.status;
        break;

      case constants.CHANGE_PASSWORD_FAILED:
        break;

      default:
        break;
    }
  });

export default accountReducer;
