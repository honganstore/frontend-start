/*
 *
 * App actions
 *
 */

import * as constants from './constants';

export function checkLogin() {
  return {
    type: constants.CHECK_LOGIN,
  };
}

export function checkLoginSuccess(status) {
  return {
    type: constants.CHECK_LOGIN_SUCCESS,
    status,
  };
}

export function checkLoginFailed(error) {
  return {
    type: constants.CHECK_LOGIN_FAILED,
    error,
  };
}

export function changePassword(requestData) {
  return {
    type: constants.CHANGE_PASSWORD,
    requestData,
  };
}

export function changePasswordSuccess(payload) {
  return {
    type: constants.CHANGE_PASSWORD_SUCCESS,
    payload,
  };
}

export function changePasswordFailed(error) {
  return {
    type: constants.CHANGE_PASSWORD_FAILED,
    error,
  };
}
