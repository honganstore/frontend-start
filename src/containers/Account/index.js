/* eslint-disable react/prefer-stateless-function */
/**
 *
 * Account
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Switch, Route } from 'react-router-dom';

import AccountContent from './screens/AccountContent/Loadable';
import ChangePassword from './screens/ChangePassword/Loadable';
import * as actions from './actions';

export class Account extends React.Component {
  componentDidMount() {
    this.props.dispatch(actions.checkLogin());
  }

  render() {
    // console.log('Main Props:', this.props);
    const { match } = this.props;
    return (
      <div>
        <>
          <Switch>
            <Route exact path={`${match.path}`} component={AccountContent} />
            <Route
              exact
              path={`${match.path}/changePassword`}
              component={ChangePassword}
            />
            <Redirect to="/" />
          </Switch>
        </>
      </div>
    );
  }
}

Account.propTypes = {
  dispatch: PropTypes.func.isRequired,
  match: PropTypes.any,
};

export default Account;
