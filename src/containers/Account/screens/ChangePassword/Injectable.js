/* eslint-disable no-dupe-keys */
import { withFormik } from 'formik';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import * as Yup from 'yup';

import * as actions from '../../actions';
import ChangePassword from '.';

import { makeSelectCurrentUser } from '../../../Home/selectors';

const mapStateToProps = createStructuredSelector({
  currentUser: makeSelectCurrentUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
  withFormik({
    mapPropsToValues: () => ({
      user_id: '',
      password: '',
      newPassword: '',
      rePassword: '',
    }),
    validationSchema: Yup.object().shape({
      password: Yup.string().required('Mật khẩu không được để trống'),
      newPassword: Yup.string()
        .notOneOf(
          [Yup.ref('password'), null],
          'Mật khẩu mới không được để trống hoặc trùng với mật khẩu cũ.',
        )
        .required('Mật khẩu không được để trống')
        .matches(
          /^(?=.*[a-z])(?=.*[0-9])(?=.{8,})/,
          'Mật khẩu tối thiểu tám ký tự, bắt buộc bao gồm chữ cái và số.',
        ),
      rePassword: Yup.string()
        .required('Mật khẩu không được để trống.')
        .oneOf(
          [Yup.ref('newPassword'), null],
          'Mật khẩu nhập lại không trùng khớp.',
        ),
    }),
    handleSubmit: (values, formik) => {
      const { props } = formik;
      const { dispatch } = props;
      // console.log('Change password:', values);
      dispatch(actions.changePassword(values));
    },
  }),
)(ChangePassword);
