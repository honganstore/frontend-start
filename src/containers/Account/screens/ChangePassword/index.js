/* eslint-disable react/no-unused-state */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Row, Col } from 'reactstrap';
import { Form, Formik } from 'formik';
import PropTypes from 'prop-types';
import _ from 'lodash';
import FormInput from '../../../../components/Form/FormInput';

// Style of class used from 'containers/Account/screens/AccountContent/index.js'
import AccountNavigation from '../../components/AccountNavigation';
import {
  AccountContainer,
  Title,
  AccountFields,
  AccountFieldsContainers,
  FlexDiv,
  SaveButton,
  SectionOdd,
  Container,
  AccountWrapper,
  AccountDisplay,
  AccountFormWrapper,
} from '../AccountContent';

import * as globalActions from '../../../App/actions';

const FlexDivPassword = styled(FlexDiv)`
  position: relative;
`;

export default class ChangePassword extends React.Component {
  getSnapshotBeforeUpdate(prevs) {
    if (!prevs.currentUser.user_id && this.props.currentUser.user_id) {
      return true;
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (snapshot !== null || snapshot === true) {
      const id = _.get(this.props, 'currentUser.user_id', '');
      this.props.setFieldValue('user_id', id);
    }
  }

  componentDidMount() {
    this.props.dispatch(globalActions.getAccountInfo());
    const id = _.get(this.props, 'currentUser.user_id', '');
    this.props.setFieldValue('user_id', id);
  }

  render() {
    const {
      values,
      touched,
      handleChange,
      handleSubmit,
      errors,
      location,
    } = this.props;
    // console.log('password props', this.props);
    return (
      <>
        <Helmet>
          <title>Account</title>
          <meta name="description" content="Description of Account" />
        </Helmet>
        <SectionOdd>
          <Container>
            <AccountWrapper>
              <AccountDisplay>
                <Row className="w-100 m-0 p-0">
                  <Col xs={12} md={4} lg={3} xl={2}>
                    <AccountNavigation location={location} />
                  </Col>
                  <Col xs={12} md={8} lg={9} xl={10}>
                    <AccountFormWrapper>
                      <AccountContainer>
                        <Title>Đổi mật khẩu</Title>
                        <Formik>
                          <Form>
                            <AccountFields>
                              <AccountFieldsContainers>
                                <FlexDivPassword xs={12}>
                                  <FormInput
                                    name="user_id"
                                    type="hidden"
                                    placeholder=""
                                    bind
                                    maxLength="30"
                                    value={values.user_id}
                                  />
                                </FlexDivPassword>
                                <FlexDivPassword xs={12}>
                                  <FormInput
                                    name="password"
                                    title="Mật khẩu hiện tại"
                                    type="password"
                                    placeholder=""
                                    bind
                                    maxLength="30"
                                    value={values.password}
                                    touched={_.get(touched, 'password')}
                                    onChange={handleChange('password')}
                                    errorMessage={_.get(errors, 'password', '')}
                                  />
                                </FlexDivPassword>
                                <FlexDivPassword xs={12}>
                                  <>
                                    <FormInput
                                      name="newPassword"
                                      title="Mật khẩu mới"
                                      type="password"
                                      placeholder=""
                                      bind
                                      maxLength="30"
                                      value={values.newPassword}
                                      touched={_.get(touched, 'newPassword')}
                                      onChange={handleChange('newPassword')}
                                      errorMessage={_.get(
                                        errors,
                                        'newPassword',
                                        '',
                                      )}
                                    />
                                  </>
                                </FlexDivPassword>
                                <FlexDivPassword xs={12}>
                                  <>
                                    <FormInput
                                      name="rePassword"
                                      title="Xác nhận lại mật khẩu mới"
                                      type="password"
                                      placeholder=""
                                      bind
                                      maxLength="30"
                                      value={values.rePassword}
                                      touched={_.get(touched, 'rePassword')}
                                      onChange={handleChange('rePassword')}
                                      errorMessage={_.get(
                                        errors,
                                        'rePassword',
                                        '',
                                      )}
                                    />
                                  </>
                                </FlexDivPassword>
                              </AccountFieldsContainers>
                              <SaveButton type="submit" onClick={handleSubmit}>
                                Đặt lại mật khẩu
                              </SaveButton>
                            </AccountFields>
                          </Form>
                        </Formik>
                      </AccountContainer>
                    </AccountFormWrapper>
                  </Col>
                </Row>
              </AccountDisplay>
            </AccountWrapper>
          </Container>
        </SectionOdd>
      </>
    );
  }
}

ChangePassword.propTypes = {
  // updatePasswordMessage: PropTypes.any,
  dispatch: PropTypes.func,
  currentUser: PropTypes.any,
  setFieldValue: PropTypes.any,
  values: PropTypes.any,
  touched: PropTypes.any,
  handleChange: PropTypes.any,
  handleSubmit: PropTypes.any,
  errors: PropTypes.any,
  location: PropTypes.any,
};
