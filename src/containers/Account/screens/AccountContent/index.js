/* eslint-disable react/prefer-stateless-function */
/**
 *
 * Account
 *
 */
import React from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import { Prompt } from 'react-router-dom';
import styled from 'styled-components';
import { Row, Col, Button } from 'reactstrap';
import { Form } from 'formik';
import _ from 'lodash';

import AccountNavigation from '../../components/AccountNavigation';
import FormInput from '../../../../components/Form/FormInput';
// import FullpageLoader from 'components/FullpageLoader';
import * as globalActions from '../../../App/actions';

// export used by 'containers/Account/Screen/ChangePassword/index.js'
export const SectionOdd = styled.section`
  padding: 30px 0 60px;
`;

export const Container = styled.div`
  max-width: 1280px;
  width: 100%;
  margin-top: 48px !important;
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
`;

export const AccountWrapper = styled.div`
  color: #333;
  font-family: open sans, sans-serif !important;
  font-size: 14px;
  font-weight: 400;
  letter-spacing: 0.8px;
  line-height: 1.5px;
  margin: 0;
  padding: 0;
`;

export const AccountDisplay = styled.div`
  display: flex;
`;

export const AccountFormWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

// Account Container
export const AccountContainer = styled.div`
  width: 100%;
  line-height: 1.5;
  display: block;
  margin-top: 0;
`;

export const Title = styled.h3`
  font-size: 26px;
  font-weight: 600;
  margin-bottom: 16px;
  text-align: left;
  letter-spacing: 1.5px;
`;

// Wrap Form
export const AccountFields = styled.div`
  font-size: 14px;
`;

export const AccountFieldsContainers = styled.div`
  font-size: 14px;
  text-align: left;
`;

// Wrap 2 input forms
export const FlexDiv = styled(Row)`
  display: flex;
  margin-bottom: 16px;
  padding: 0;
`;

export const SaveButton = styled(Button)`
  height: 50px;
  background-color: white;
  color: black;
  border-color: unset;
  margin: 10px 0 0 0;
  padding: 10px;
  &:hover,
  &:active,
  &:focus {
    background-color: black !important;
    color: white;
    border-color: unset;
    box-shadow: none;
  }
`;

export class AccountContent extends React.Component {
  componentDidMount() {
    this.props.dispatch(globalActions.getAccountInfo());
    if (this.props.currentUser) {
      this.props.setValues({
        id: _.get(this.props, 'currentUser.user_id', ''),
        email: _.get(this.props, 'currentUser.email', ''),
        first_name: _.get(this.props, 'currentUser.first_name', ''),
        last_name: _.get(this.props, 'currentUser.last_name', ''),
        role: _.get(this.props, 'currentUser.role', ''),
        phone: _.get(this.props, 'currentUser.phone', ''),
        address: _.get(this.props, 'currentUser.address', ''),
      });
    }
  }

  getSnapshotBeforeUpdate(prevProps) {
    if (
      this.handleValuesDiff(prevProps.currentUser, this.props.currentUser) ===
      false
    ) {
      return true;
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (snapshot !== null || snapshot === true) {
      this.props.setValues({
        id: _.get(this.props, 'currentUser.user_id', ''),
        email: _.get(this.props, 'currentUser.email', ''),
        first_name: _.get(this.props, 'currentUser.first_name', ''),
        last_name: _.get(this.props, 'currentUser.last_name', ''),
        role: _.get(this.props, 'currentUser.role', ''),
        phone: _.get(this.props, 'currentUser.phone', ''),
        address: _.get(this.props, 'currentUser.address', ''),
      });
    }
  }

  handleValuesDiff = (currentUser, props) => {
    const model = {
      last_name: null,
      first_name: null,
      phone: null,
      email: null,
      address: null,
    };
    const currentResult = _.pick(currentUser, _.keys(model));
    // debugger;
    // console.log('currentUser', currentUser);
    // console.log('currentResult', currentResult);
    const propsResult = _.pick(props, _.keys(model));
    // console.log('propsResult', propsResult);
    // console.log(_.isEqual(currentResult, propsResult));
    return _.isEqual(currentResult, propsResult);
  };

  render() {
    const {
      errors,
      values,
      touched,
      handleChange,
      handleSubmit,
      currentUser,
      location,
    } = this.props;

    // console.log(_.isEqual(currentUser, values));
    return (
      <>
        <Prompt
          when={this.handleValuesDiff(currentUser, values) === false}
          message="Are you sure you want to leave?"
        />
        <Helmet>
          <title>Account</title>
          <meta name="description" content="Description of Account" />
        </Helmet>
        <SectionOdd>
          <Container>
            <AccountWrapper>
              <AccountDisplay>
                <Row className="w-100 m-0 p-0">
                  <Col xs={12} md={4} lg={3} xl={2}>
                    <AccountNavigation location={location} />
                  </Col>
                  <Col xs={12} md={8} lg={9} xl={10}>
                    <AccountFormWrapper>
                      <AccountContainer>
                        <Title>Thông tin</Title>
                        <AccountFields>
                          <AccountFieldsContainers>
                            <Form>
                              <FlexDiv>
                                <Col xs={12} md={6} lg={6}>
                                  <FormInput
                                    name="lastName"
                                    title="Họ"
                                    type="text"
                                    placeholder=""
                                    require
                                    value={values.last_name}
                                    touched={_.get(touched, 'last_name')}
                                    onChange={handleChange('last_name')}
                                    errorMessage={_.get(
                                      errors,
                                      'last_name',
                                      '',
                                    )}
                                  />
                                </Col>
                                <Col xs={12} md={6} lg={6}>
                                  <FormInput
                                    name="firstName"
                                    title="Tên"
                                    type="text"
                                    placeholder=""
                                    require
                                    value={values.first_name}
                                    touched={_.get(touched, 'first_name')}
                                    onChange={handleChange('first_name')}
                                    errorMessage={_.get(
                                      errors,
                                      'first_name',
                                      '',
                                    )}
                                  />
                                </Col>
                              </FlexDiv>
                              <FlexDiv>
                                <Col xs={12} md={6} lg={6}>
                                  <FormInput
                                    name="phone"
                                    title="Số điện thoại"
                                    type="text"
                                    placeholder=""
                                    require
                                    value={values.phone}
                                    touched={_.get(touched, 'phone')}
                                    onChange={handleChange('phone')}
                                    errorMessage={_.get(errors, 'phone', '')}
                                  />
                                </Col>
                                <Col xs={12} md={6} lg={6}>
                                  <FormInput
                                    name="email"
                                    title="Email"
                                    type="text"
                                    placeholder=""
                                    require
                                    value={values.email}
                                    touched={_.get(touched, 'email')}
                                    onChange={handleChange('email')}
                                    errorMessage={_.get(errors, 'email', '')}
                                    disabled
                                  />
                                </Col>
                              </FlexDiv>
                              <FlexDiv>
                                <Col xs={12}>
                                  <FormInput
                                    name="address"
                                    title="Địa chỉ"
                                    type="text"
                                    placeholder=""
                                    value={values.address}
                                    touched={_.get(touched, 'address')}
                                    onChange={handleChange('address')}
                                    errorMessage={_.get(errors, 'address', '')}
                                  />
                                </Col>
                              </FlexDiv>
                            </Form>
                          </AccountFieldsContainers>
                          <SaveButton type="submit" onClick={handleSubmit}>
                            Lưu thông tin
                          </SaveButton>
                        </AccountFields>
                      </AccountContainer>
                    </AccountFormWrapper>
                  </Col>
                </Row>
              </AccountDisplay>
            </AccountWrapper>
          </Container>
        </SectionOdd>
        {/* {hasLoader.isLoading && (
          <FullpageLoader msg={_.get(hasLoader, 'msg')} />
        )} */}
      </>
    );
  }
}

AccountContent.propTypes = {
  dispatch: PropTypes.func.isRequired,
  currentUser: PropTypes.any,
  setValues: PropTypes.any,
  errors: PropTypes.any,
  values: PropTypes.any,
  touched: PropTypes.any,
  handleChange: PropTypes.any,
  handleSubmit: PropTypes.any,
  location: PropTypes.any,
  // hasLoader: PropTypes.any,
};

export default AccountContent;
