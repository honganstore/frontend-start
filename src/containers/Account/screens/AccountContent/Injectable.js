import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { withFormik } from 'formik';
import * as Yup from 'yup';
import * as globalActions from '../../../App/actions';

import UpdateAccountFormToRequest from '../../mappers/UpdateAccountFormToRequest';

import AccountContent from '.';
import { makeSelectCurrentUser } from '../../../Home/selectors';
const mapStateToProps = createStructuredSelector({
  currentUser: makeSelectCurrentUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

export default compose(
  withConnect,
  withFormik({
    mapPropsToValues: () => ({}),
    validationSchema: Yup.object().shape({
      last_name: Yup.string().required('Họ không được để trống.'),
      first_name: Yup.string().required('Tên không được để trống.'),
      phone: Yup.string()
        .required('Số điện thoại không được để trống.')
        .min(10, 'Số điện thoại ít nhất 10 số.')
        .max(11, 'Số điện thoại tối đa 11 số.')
        .matches(phoneRegExp, 'Số điện thoại không hợp lệ.'),
      email: Yup.string()
        .email('Địa chỉ email không hợp lệ.')
        .required('Email không được để trống.'),
      address: Yup.string().nullable(),
    }),
    handleSubmit: (values, formik) => {
      const { props } = formik;
      const { dispatch } = props;
      // console.log(values);
      const requestData = UpdateAccountFormToRequest(values);
      // console.log(requestData);
      dispatch(globalActions.updateAccountInfo(requestData));
      // console.log(values);
    },
  }),
)(AccountContent);
