const ChangePasswordFormToRequest = (values) => {
  const requestData = {
    oldPassword: values.password,
    newPassword: values.newPassword,
  };
  return requestData;
};

export default ChangePasswordFormToRequest;
