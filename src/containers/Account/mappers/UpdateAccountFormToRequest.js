import _ from 'lodash';

const UpdateAccountFormToRequest = (values) => {
  const requestData = {
    user_id: values.id,
    email: values.email,
    first_name: values.first_name,
    last_name: values.last_name,
    address: values.address || ' ',
    phone: values.phone,
  };

  _.each(requestData, (value, key) => {
    if (!value && typeof value !== 'boolean' && typeof value !== 'number') {
      delete requestData[key];
    }
  });

  return requestData;
};

export default UpdateAccountFormToRequest;
