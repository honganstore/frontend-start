/*
 *
 * Account constants
 *
 */

export const CHECK_LOGIN = 'w/Account/CHECK_LOGIN';
export const CHECK_LOGIN_SUCCESS = 'w/Account/CHECK_LOGIN_SUCCESS';
export const CHECK_LOGIN_FAILED = 'w/Account/CHECK_LOGIN_FAILED';

export const CHANGE_PASSWORD = 'w/Account/CHANGE_PASSWORD';
export const CHANGE_PASSWORD_SUCCESS = 'w/Account/CHANGE_PASSWORD_SUCCESS';
export const CHANGE_PASSWORD_FAILED = 'w/Account/CHANGE_PASSWORD_FAILED';
