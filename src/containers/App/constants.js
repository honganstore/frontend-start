/*
 *
 * App constants
 *
 */

export const REGISTER = 'w/App/REGISTER';
export const REGISTER_SUCCESS = 'w/App/REGISTER_SUCCESS';
export const REGISTER_FAILED = 'w/App/REGISTER_FAILED';

export const LOGIN = 'w/App/LOGIN';
export const LOGIN_SUCCESS = 'w/App/LOGIN_SUCCESS';
export const LOGIN_FAILED = 'w/App/LOGIN_FAILED';

export const LOGOUT = 'w/App/LOGOUT';

export const GET_ACCOUNT_INFO = 'w/App/GET_ACCOUNT_INFO';
export const GET_ACCOUNT_INFO_SUCCESS = 'w/App/GET_ACCOUNT_INFO_SUCCESS';
export const GET_ACCOUNT_INFO_FAILED = 'w/App/GET_ACCOUNT_INFO_FAILED';

export const UPDATE_ACCOUNT_INFO = 'w/Account/UPDATE_ACCOUNT_INFO';
export const UPDATE_ACCOUNT_INFO_SUCCESS =
  'w/Account/UPDATE_ACCOUNT_INFO_SUCCESS';
export const UPDATE_ACCOUNT_INFO_FAILED =
  'w/Account/UPDATE_ACCOUNT_INFO_FAILED';
