/*
 *
 * App actions
 *
 */

import * as constants from './constants';

export function register(request) {
  return {
    type: constants.REGISTER,
    request,
  };
}

export function registerSuccess(payload) {
  return {
    type: constants.REGISTER_SUCCESS,
    payload,
  };
}

export function registerFailed(error) {
  return {
    type: constants.REGISTER_FAILED,
    error,
  };
}

export function login(request) {
  return {
    type: constants.LOGIN,
    request,
  };
}

export function loginSuccess(payload) {
  return {
    type: constants.LOGIN_SUCCESS,
    payload,
  };
}

export function loginFailed(error) {
  return {
    type: constants.LOGIN_FAILED,
    error,
  };
}

export function logout() {
  return {
    type: constants.LOGOUT,
  };
}

export function getAccountInfo() {
  return {
    type: constants.GET_ACCOUNT_INFO,
  };
}

export function getAccountInfoSuccess(payload) {
  return {
    type: constants.GET_ACCOUNT_INFO_SUCCESS,
    payload,
  };
}

export function getAccountInfoFailed(error) {
  return {
    type: constants.GET_ACCOUNT_INFO_FAILED,
    error,
  };
}

export function updateAccountInfo(requestData) {
  return {
    type: constants.UPDATE_ACCOUNT_INFO,
    requestData,
  };
}

export function updateAccountInfoSuccess(payload) {
  return {
    type: constants.UPDATE_ACCOUNT_INFO_SUCCESS,
    payload,
  };
}

export function updateAccountInfoFailed(error) {
  return {
    type: constants.UPDATE_ACCOUNT_INFO_FAILED,
    error,
  };
}
