/*
 *
 * App reducer
 *
 */
import produce from 'immer';
import _ from 'lodash';
import * as constants from './constants';

import { getAccessToken } from '../../utils/request';

export const initialState = {
  access_token: getAccessToken(),
  isLogin: false,
  currentUser: JSON.parse(localStorage.getItem('current_user')) || {},
};

/* eslint-disable default-case, no-param-reassign */
const appReducer = (state = initialState, action) =>
  produce(state, (newState) => {
    switch (action.type) {
      case constants.REGISTER:
        break;

      case constants.REGISTER_SUCCESS:
        break;

      case constants.REGISTER_FAILED:
        break;

      case constants.LOGIN:
        break;

      case constants.LOGIN_SUCCESS:
        newState.access_token = _.get(action, 'payload.token', '');
        newState.isLogin = true;
        break;

      case constants.LOGIN_FAILED:
        newState.isLogin = false;
        break;

      case constants.LOGOUT:
        localStorage.removeItem('access_token');
        newState.access_token = null;
        newState.isLogin = false;
        break;

      case constants.GET_ACCOUNT_INFO:
        break;

      case constants.GET_ACCOUNT_INFO_SUCCESS:
        newState.isLogin = true;
        newState.currentUser = action.payload;
        break;

      case constants.GET_ACCOUNT_INFO_FAILED:
        newState.isLogin = false;
        break;

      case constants.UPDATE_ACCOUNT_INFO:
        break;

      case constants.UPDATE_ACCOUNT_INFO_SUCCESS:
        newState.isLogin = true;
        newState.currentUser = action.payload;
        break;

      case constants.UPDATE_ACCOUNT_INFO_FAILED:
        newState.isLogin = false;
        break;

      default:
        break;
    }
  });

export default appReducer;
