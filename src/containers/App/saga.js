import { takeLatest, call, put } from 'redux-saga/effects';
import { push } from 'connected-react-router';
import { toast } from 'react-toastify';
import * as constants from './constants';
import * as actions from './actions';
import * as modalActions from '../Modals/actions';
import config from '../../global-config';
import request, { storeToken } from '../../utils/request';

function* onRegister(action) {
  // console.log('go saga');
  // console.log('action', action);
  // console.log('json action', JSON.stringify({ ...action.request }));
  const requestURL = `${config.baseUrl}/signup`;
  try {
    const res = yield call(request, requestURL, {
      method: 'POST',
      body: JSON.stringify({ ...action.request }),
    });
    yield put(modalActions.showRegisterModals(false));
    toast.success('Bạn đã đăng ký tài khoản thành công');
    yield put(actions.registerSuccess(res));
  } catch (error) {
    if (error.status === 400) {
      toast.warn('Email này đã được sử dụng');
    }
    yield put(actions.registerFailed(error));
  }
}

function* onLogin(action) {
  // console.log('go saga');
  // console.log('action', action);
  // console.log('json action', JSON.stringify({ ...action.request }));
  const requestURL = `${config.baseUrl}/signin`;
  try {
    const res = yield call(request, requestURL, {
      method: 'POST',
      body: JSON.stringify({ ...action.request }),
    });
    // console.log('respone', res);
    storeToken(res.token);
    yield put(modalActions.showLoginModals(false));
    yield put(push('/'));

    yield put(actions.getAccountInfo());
    toast.success('Chào mừng bạn đến với Hongan!');
    yield put(actions.loginSuccess(res));
  } catch (error) {
    toast.error('Email hoặc password không chính xác');
    yield put(actions.loginFailed(error));
  }
}

function* onLogout() {
  yield put(push('/'));
}

function* onGetAccountInfo() {
  const requestURL = `${config.apiUrl}/user/info`;
  try {
    const res = yield call(request, requestURL);
    // console.log('Get account info sucess');
    yield put(actions.getAccountInfoSuccess(res.result[0]));
  } catch (error) {
    yield put(actions.getAccountInfoFailed(error));
  }
}

function* onUpdateAccountInfo(action) {
  // console.log('go saga');
  // console.log('action', action);
  // console.log('JSON update', JSON.stringify(action.requestData));
  const requestURL = `${config.apiUrl}/user/update/${action.requestData.user_id}`;
  try {
    yield call(request, requestURL, {
      method: 'PUT',
      body: JSON.stringify(action.requestData),
    });
    toast.success('Cập nhật thông tin cá nhân thành công');
    yield put(actions.updateAccountInfoSuccess(action.requestData));
  } catch (error) {
    yield put(actions.updateAccountInfoFailed(error));
  }
}

// Individual exports for testing
export default function* appSaga() {
  yield takeLatest(constants.REGISTER, onRegister);
  yield takeLatest(constants.LOGIN, onLogin);
  yield takeLatest(constants.LOGOUT, onLogout);
  yield takeLatest(constants.GET_ACCOUNT_INFO, onGetAccountInfo);
  yield takeLatest(constants.UPDATE_ACCOUNT_INFO, onUpdateAccountInfo);
}
