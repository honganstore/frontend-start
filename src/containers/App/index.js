/**
 *
 * App
 *
 */

import React from 'react';
import { Helmet } from 'react-helmet';

import { Redirect, Route, Switch } from 'react-router-dom';
import Home from '../Home/Loadable';
import Account from '../Account/Loadable';
import Product from '../Product/Loadable';
import Modals from '../Modals/Loadable';
import Checkout from '../Checkout/Loadable';
import withMainLayout from '../layouts/MainLayout';
// import messages from './messages';
import GlobalStyle from '../../global-styles';

export function App() {
  return (
    <>
      <Helmet>
        <title>App</title>
        <meta name="description" content="Description of App" />
      </Helmet>
      <>
        <Switch>
          <Route exact path="/" component={withMainLayout(Home)} />
          <Route path="/account" component={withMainLayout(Account)} />
          <Route path="/product" component={withMainLayout(Product)} />
          <Route path="/checkout" component={withMainLayout(Checkout)} />
          {/* <Route exact path="/jobseek" component={JobSeek} />
          <Route exact path="/searchresults" component={SearchResults} />
          <Route exact path="/talent" component={Talent} /> */}
          <Redirect to="/" />
        </Switch>
      </>
      <GlobalStyle />
      <Modals />
    </>
  );
}

App.propTypes = {
  // dispatch: PropTypes.func,
};

export default App;
