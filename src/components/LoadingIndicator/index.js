import React from 'react';
import styled from 'styled-components';
import { Spinner } from 'reactstrap';

const LoadingContainer = styled.div``;
const LoadText = styled.div``;
const Wrapper = styled.div`
  /* margin: 0 -15px; */
  position: absolute;
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  background: white;
  z-index: 2000;
  .spinner-border {
    color: black;
    width: 50px;
    height: 50px;
  }
  ${LoadingContainer} {
    width: auto !important;
    height: auto !important;
    text-align: center;
  }
  ${LoadText} {
    color: black;
  }
`;

const LoadingIndicator = () => (
  <Wrapper>
    <LoadingContainer>
      <Spinner />
      {/* <LoadText>Loading...</LoadText> */}
    </LoadingContainer>
  </Wrapper>
);

export default LoadingIndicator;
