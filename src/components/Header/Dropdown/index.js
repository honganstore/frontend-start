/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

import * as globalActions from '../../../containers/App/actions';

const DropdownWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  .dropdownItem {
    color: black;
    padding: 0;
    margin: 5px 0 5px 5px;
    text-align: left;
    &:hover {
      text-decoration: underline;
    }
  }
`;

const StyledNav = styled(NavLink)`
  color: black;
  padding: 0;
  margin: 5px 0 5px 5px;
  text-align: left;
`;
class Dropdown extends React.Component {
  handleLogout = () => {
    this.props.dispatch(globalActions.logout());
  };

  render() {
    // console.log('Dropdown Props:', this.props);
    return (
      <DropdownWrapper>
        <StyledNav to="account" activeClassName="active">
          Thông tin tài khoản
        </StyledNav>
        <div className="dropdownItem" onClick={this.handleLogout}>
          Đăng xuất
        </div>
      </DropdownWrapper>
    );
  }
}

Dropdown.propTypes = {
  dispatch: PropTypes.any,
};

export default Dropdown;
