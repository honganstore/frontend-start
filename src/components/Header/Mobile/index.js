/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { Button, Modal, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { MdMenu } from 'react-icons/md';
import styled, { keyframes } from 'styled-components';

const moveLeft = keyframes`
    0% {
    opacity: 0;
    transform: translateX(150px);
  }
  20% {
    opacity: 1;
    transform: translateX(0px);
  }
  85% {
    opacity: 1;
    transform: translateX(0px);
  }
  100% {
    opacity: 0;
    transform: translateX(-30px);
  }
`;

const Hamberger = styled.div`
  position: relative;
  width: 100px;
  height: 38px;
  .menu-mobile {
    position: absolute;
    width: 100%;
    background-color: white;
    box-shadow: 0px 0px 4px 2px;
    padding: 15px;
    top: 38px;
    display: none;
    transition: ease-in-out 0.6;
    ul {
      list-style-type: none;
      padding: 0;
      margin: 0;
      ._link {
        color: black;
      }
    }
  }

  &:hover {
    .menu-mobile {
      display: block;
      animation: ${moveLeft} 3s ease-in;
    }
  }
`;

const StyledButton = styled(Button)`
  position: absolute;
  right: 0;
  &:focus,
  &:active {
    box-shadow: unset;
  }
  svg {
    font-size: 28px;
  }
`;

function TopbarMobileRender() {
  return (
    <Hamberger>
      <StyledButton variant="none">
        <MdMenu />
      </StyledButton>
      <div className="menu-mobile">
        <ul>
          <li>
            <Link className="_link" to="/category">
              Tất cả sản phẩm
            </Link>
          </li>
          <li>
            <Link className="_link" to="/category">
              Áo
            </Link>
          </li>
          <li>
            <Link className="_link" to="/category">
              Váy
            </Link>
          </li>
          <li>
            <Link className="_link" to="/category">
              Short
            </Link>
          </li>
          <li>
            <Link className="_link" to="/category">
              Jeans
            </Link>
          </li>
          <li>
            <Link className="_link" to="/category">
              Đầm
            </Link>
          </li>
          <li>
            <Link className="_link" to="/category">
              Jumsuit
            </Link>
          </li>
        </ul>
      </div>
    </Hamberger>
  );
}

export default TopbarMobileRender;
