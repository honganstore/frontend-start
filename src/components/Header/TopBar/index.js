/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/prop-types */
import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from '../../../assets/header/topbar.module.css';
import TopbarMobileRender from '../Mobile';

function TopBarRender() {
  return <TopBar data={dataMainMenu} />;
}

function TopBar({ data }) {
  return (
    <nav>
      <div className={styles.topBar}>
        <TopBarUl {...data} />
        <div className={styles.topBarMobile}>
          <TopbarMobileRender />
        </div>
      </div>
    </nav>
  );
}

function TopBarUl({ kieu1, kieu2, kieu3, kieu4, kieu5, kieu6, kieu7 }) {
  return (
    <ul className={styles.topBarDesktop}>
      <li>
        <NavLink to="/product" activeClassName="active">
          {kieu7}
        </NavLink>
      </li>
      <li>
        <NavLink to="/category" activeClassName="active">
          {kieu1}
        </NavLink>
      </li>
      <li>
        <NavLink to="/category" activeClassName="active">
          {kieu2}
        </NavLink>
      </li>
      <li>
        <NavLink to="/category" activeClassName="active">
          {kieu3}
        </NavLink>
      </li>
      <li>
        <NavLink to="/category" activeClassName="active">
          {kieu4}
        </NavLink>
      </li>
      <li>
        <NavLink to="/category" activeClassName="active">
          {kieu5}
        </NavLink>
      </li>
      <li>
        <NavLink to="/category" activeClassName="active">
          {kieu6}
        </NavLink>
      </li>
    </ul>
  );
}

const dataMainMenu = {
  kieu1: 'Áo',
  kieu2: 'Váy',
  kieu3: 'Shorts',
  kieu4: 'Jeans',
  kieu5: 'Đầm',
  kieu6: 'Jumpsuit',
  kieu7: 'Tất cả sản phẩm',
};

export default TopBarRender;
