/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import styles from '../../../../assets/header/header.module.css';

import MiniCartItem from '../CartItem';
import * as productActions from '../../../../containers/Product/actions';

const StyledLink = styled(Link)`
  &:hover,
  &:active,
  &:focus {
    color: ${(props) => (props.shopping ? 'white' : 'black')};
  }
`;

class MiniCart extends React.Component {
  handleDeleteItem = (id) => {
    // console.log(id);
    const product = _.get(this.props, 'cartItem.cartItemList', []).find(
      (variant) => id === variant.variant_id,
    );
    this.props.dispatch(productActions.deleteFromCart(product.variant_id));
  };

  render() {
    const { cartItem } = this.props;
    // console.log('Minicart props: ', this.props);
    return (
      <>
        <sup className={styles.cartCount}>{_.get(cartItem, 'count', 0)}</sup>
        <ul className={styles.miniCart}>
          {_.get(cartItem, 'cartItemList', []).map((item) => (
            <MiniCartItem
              key={item.variant_id}
              {...item}
              onDelFromCart={this.handleDeleteItem}
            />
          ))}

          <li className={styles.makeLi}>
            <div className={styles.totalPrice}>
              <p>
                <span className="f-left">Count:</span>
                <span className="f-right">{_.get(cartItem, 'count', 0)}</span>
              </p>
              <span className="f-left">Total:</span>
              <span className="f-right">
                {new Intl.NumberFormat('de-DE', {
                  style: 'currency',
                  currency: 'VND',
                }).format(_.get(cartItem, 'totalPrice', 0))}
              </span>
            </div>
          </li>
          <li>
            <div className={styles.checkoutLink}>
              <StyledLink shopping to="/checkout">
                Shopping Cart
              </StyledLink>
              <StyledLink className={styles.redColor} to="/checkout">
                Checkout
              </StyledLink>
            </div>
          </li>
        </ul>
      </>
    );
  }
}

// total: new Intl.NumberFormat('de-DE', {
//     style: 'currency',
//     currency: 'VND',
//   }).format(cartAdd.total),

MiniCart.propTypes = {
  dispatch: PropTypes.any,
  cartItem: PropTypes.any,
};

export default MiniCart;
