/* eslint-disable camelcase */
import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { FaTrashAlt } from 'react-icons/fa';
import styles from '../../../../assets/header/header.module.css';

export default class MinicartItem extends React.Component {
  handleDeleteClick = (event) => {
    event.preventDefault();
    // console.log('ID delete:', this.props.variant_id);
    this.props.onDelFromCart(this.props.variant_id);
  };

  render() {
    const {
      cloth_name,
      size,
      color,
      price,
      quantity,
      img_1,
      cloth_id,
    } = this.props;
    console.log('Props Cart:', this.props);
    return (
      <li>
        <NavLink to={`/product/details/${cloth_id}`} activeClassName="active">
          <div className={styles.cartImg}>
            <img src={img_1} alt="Cart Cloth" />
          </div>
          <div className={styles.cartContent}>
            <h3>{cloth_name}</h3>
            <div className={styles.cartPrice}>
              <span className="new">
                {new Intl.NumberFormat('de-DE', {
                  style: 'currency',
                  currency: 'VND',
                }).format(price)}
              </span>
              <strong>x {quantity}</strong>
            </div>
            <div className={styles.cartPrice}>
              <span>Size: {size}</span>{' '}
              <span>
                Color: <span className={`${styles.checkMark} ${color}`}></span>
              </span>
            </div>
          </div>
          <div className={styles.delIcon}>
            <FaTrashAlt onClick={this.handleDeleteClick} />
          </div>
        </NavLink>
      </li>
    );
  }
}

MinicartItem.propTypes = {
  cloth_name: PropTypes.any,
  price: PropTypes.any,
  quantity: PropTypes.any,
  img_1: PropTypes.any,
  cloth_id: PropTypes.any,
  size: PropTypes.any,
  color: PropTypes.any,
  variant_id: PropTypes.any,
  onDelFromCart: PropTypes.func,
};
