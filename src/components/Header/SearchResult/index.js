/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from '../../../assets/cart/minicart.module.css';
export default class SearchResult extends React.Component {
  render() {
    const { id, img, title, price } = this.props;
    // console.log('Props Search:', this.props);
    return (
      <NavLink to={`/details/${id}`} activeClassName="active">
        <li>
          <div className={styles.cartImg}>
            <a href="#">
              <img src={img} alt="" />
            </a>
          </div>
          <div className={styles.cartContent}>
            <h3>
              <a href="#">{title}</a>
            </h3>
            <div className={styles.cartPrice}>
              <span className="new">
                {new Intl.NumberFormat('de-DE', {
                  style: 'currency',
                  currency: 'VND',
                }).format(price)}
              </span>
            </div>
          </div>
        </li>
      </NavLink>
    );
  }
}
