/* eslint-disable react/no-unused-state */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';
import _ from 'lodash';
import { BsSearch } from 'react-icons/bs';
import { AiFillFacebook } from 'react-icons/ai';
import { FiShoppingCart } from 'react-icons/fi';
import styles from '../../../assets/header/header.module.css';
import stylesMiniCart from '../../../assets/cart/minicart.module.css';
import stylesTopbar from '../../../assets/header/topbar.module.css';
import Minicart from '../Cart/MiniCart/Injectable';
import SearchResult from '../SearchResult';
// import { searchactions } from "../../action/searcActions";

import * as modalsActions from '../../../containers/Modals/actions';

import Dropdown from '../Dropdown/Injectable';

const MenuBarItem = styled.div`
  cursor: pointer;
`;

const StyledNavLink = styled(NavLink)`
  color: black;
  text-decoration: none;
  &:hover,
  :focus,
  :active {
    color: black;
    text-decoration: none;
  }
  svg {
    margin: 0 0 0 5px;
    font-size: 22px;
  }
`;

const UserWrapper = styled.li`
  position: relative;
  border: 1px solid;
  background: black;
  color: white;
  border-radius: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 157px;
  height: 50px;
  padding: 3px 0 0 0;
  &:hover {
    text-decoration: underline;
  }
  .onHover {
    position: absolute;
    z-index: 1000;
    width: 100%;
    display: none;
  }
  &:hover .onHover {
    display: block;
    left: 0;
    background: white;
    border-radius: 0 0 5px 5px;
  }
`;

const User = styled.div`
  cursor: pointer;
  color: white;
  font-size: 14px;
`;

class HeadMenuRender extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      input: ' ',
    };
    this.search = this.search.bind(this);
  }

  dataHeadder = {
    trangChu: 'Trang chủ',
    tapChi: 'Tạp chí',
    theoDoiDonHang: 'Đơn hàng',
    dangNhap: 'Đăng nhập',
    dangKi: 'Đăng kí',
    troGiup: 'Trợ giúp',
  };

  search(e) {
    this.setState({
      input: e.target.value,
    });
    // this.props.searchactions(e.target.value);
  }

  handleLogin = () => {
    this.props.dispatch(modalsActions.showLoginModals(true));
  };

  handleRegister = () => {
    this.props.dispatch(modalsActions.showRegisterModals(true));
  };

  render() {
    // console.log('Header Props:', this.props);
    const { searchResult, isLogin, currentUser } = this.props;
    const isLoginHandler = () =>
      isLogin ? (
        <UserWrapper>
          Xin chào!
          <User>
            {`${_.get(currentUser, 'first_name', '')} ${_.get(
              currentUser,
              'last_name',
              '',
            )}`}
            <div className="onHover">
              <Dropdown />
            </div>
          </User>
        </UserWrapper>
      ) : null;
    const loginNowHanler = () =>
      isLogin ? null : (
        <li>
          <MenuBarItem onClick={this.handleLogin}>
            {this.dataHeadder.dangNhap}
          </MenuBarItem>
        </li>
      );
    return (
      <div className={styles.headMenu}>
        <NavLink to="/" activeClassName="active">
          <div className={stylesTopbar.topBarLogo}>
            <h3>honganstore</h3>
          </div>
        </NavLink>
        <ul>
          <li>
            <StyledNavLink to="/" activeClassName="active">
              {this.dataHeadder.trangChu}
            </StyledNavLink>
          </li>
          {loginNowHanler()}
          {isLogin ? null : (
            <li>
              <MenuBarItem onClick={this.handleRegister}>
                {this.dataHeadder.dangKi}
              </MenuBarItem>
            </li>
          )}
          <li>
            <StyledNavLink to="/" activeClassName="active">
              {this.dataHeadder.troGiup}
            </StyledNavLink>
          </li>
          <li>
            <StyledNavLink to="/product" activeClassName="active">
              Tất cả sản phẩm
            </StyledNavLink>
          </li>
          <li>
            <input
              name="inputSearch"
              onChange={this.search}
              placeholder="Tìm kiếm sản phẩm"
            />
            <ul className={stylesMiniCart.searchResult}>
              {[] ||
                searchResult.map((element) => <SearchResult {...element} />)}
            </ul>
            <StyledNavLink to="/" activeClassName="active">
              <BsSearch />
            </StyledNavLink>
          </li>
          <li>
            <StyledNavLink to="/" activeClassName="active">
              <AiFillFacebook />
            </StyledNavLink>
          </li>
          <li className={styles.cartIconLink}>
            <StyledNavLink to="/cart" activeClassName="active">
              <FiShoppingCart />
              <Minicart />
            </StyledNavLink>
          </li>
          {isLoginHandler()}
        </ul>
      </div>
    );
  }
}

HeadMenuRender.propTypes = {
  dispatch: PropTypes.func,
  searchResult: PropTypes.any,
  isLogin: PropTypes.any,
  currentUser: PropTypes.any,
};

export default HeadMenuRender;
