import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import {
  makeSelectIsLogin,
  makeSelectCurrentUser,
} from '../../../containers/Home/selectors';
import MenuBar from '.';

const mapStateToProps = createStructuredSelector({
  isLogin: makeSelectIsLogin(),
  currentUser: makeSelectCurrentUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(MenuBar);
