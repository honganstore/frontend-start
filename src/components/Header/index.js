import React from 'react';

import HeaderMenuRender from './Menubar/Injectable';
// import TopBarRender from './TopBar';

function Header() {
  return (
    <header>
      <HeaderMenuRender />
      {/* <TopBarRender /> */}
    </header>
  );
}

export default Header;
