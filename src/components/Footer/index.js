import React from 'react';

import SubFooterRender from './SubFooter';
import FooterEndRender from './EndFooter';

function Footer() {
  return (
    <footer>
      <SubFooterRender />
      <FooterEndRender />
    </footer>
  );
}
export default Footer;
