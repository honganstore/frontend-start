/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import FormError from '../FormError';

const InputWrapper = styled.div`
  width: 100%;
  padding-right: 8px;
  margin: 8px 0 0 0;
`;

const ControlLabel = styled.label`
  display: inline-block;
  margin-bottom: 8px;
  line-height: 21px;
`;

const Abbr = styled.abbr`
  cursor: help;
  border-bottom: 0;
  color: #d92727;
`;

const UserInput = styled.input`
  height: 44px;
  font-size: 14px;
  padding: 7.2px 12px;
  border-radius: 4px;
  display: block;
  width: 100%;
  padding: 0.375rem 0.75rem;
  font-size: 1rem;
  color: #495057;
  background-color: ${(props) => (props.disabled ? '#e5e5e5' : '#fff')};
  background-clip: padding-box;
  border: 1px solid;
  &:focus {
    box-shadow: none;
    border-color: #333;
    outline: none;
  }
`;

export default class FormInput extends React.Component {
  render() {
    const {
      errorMessage,
      name,
      value,
      maxLength,
      title,
      touched,
      require,
      ...restProps
    } = this.props;
    return (
      <InputWrapper>
        {!!title && (
          <>
            <ControlLabel>{title}</ControlLabel>
            {!!require && <Abbr>*</Abbr>}
          </>
        )}

        <UserInput
          name={name}
          type="text"
          value={value || ''}
          maxLength={maxLength || '100'}
          {...restProps}
        />

        {touched && <FormError errorMessage={errorMessage} />}
      </InputWrapper>
    );
  }
}

FormInput.propTypes = {
  errorMessage: PropTypes.any,
  require: PropTypes.any,
  name: PropTypes.any,
  value: PropTypes.any,
  maxLength: PropTypes.any,
  title: PropTypes.any,
  touched: PropTypes.any,
  bind: PropTypes.any,
};
