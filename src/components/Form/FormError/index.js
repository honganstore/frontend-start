import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledFormError = styled.div`
  color: #d92727;
  padding-left: 3px;
`;

export default function FormError({ errorMessage }) {
  if (errorMessage === '') {
    return null;
  }
  return <StyledFormError>{errorMessage}</StyledFormError>;
}

FormError.propTypes = {
  errorMessage: PropTypes.any,
};
