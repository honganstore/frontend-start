import _ from 'lodash';

const baseUrl =
  process.env.NODE_ENV === 'production'
    ? 'https://hongan-api.herokuapp.com'
    : 'http://localhost:5000';

const config = {
  baseUrl,
  apiUrl: `${baseUrl}/api`,
};

export default config;
